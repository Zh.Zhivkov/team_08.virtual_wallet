-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for virtual_wallet
DROP DATABASE IF EXISTS `virtual_wallet`;
CREATE DATABASE IF NOT EXISTS `virtual_wallet` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `virtual_wallet`;

-- Dumping structure for table virtual_wallet.authorities
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  KEY `FK_authorities_users` (`username`),
  CONSTRAINT `FK_authorities_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table virtual_wallet.authorities: ~0 rows (approximately)
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Dumping structure for table virtual_wallet.card
DROP TABLE IF EXISTS `card`;
CREATE TABLE IF NOT EXISTS `card` (
  `card_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_type` tinyint(4) DEFAULT NULL,
  `cardowner` varchar(50) NOT NULL,
  `expire_date` datetime DEFAULT NULL,
  PRIMARY KEY (`card_id`),
  KEY `FK_card_users` (`cardowner`),
  CONSTRAINT `FK_card_users` FOREIGN KEY (`cardowner`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table virtual_wallet.card: ~0 rows (approximately)
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
/*!40000 ALTER TABLE `card` ENABLE KEYS */;

-- Dumping structure for table virtual_wallet.transactions
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `recepient` varchar(50) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `direction` tinyint(1) NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `FK_recepient_users` (`recepient`),
  KEY `FK_sender_users` (`sender`),
  CONSTRAINT `FK_recepient_users` FOREIGN KEY (`recepient`) REFERENCES `users` (`username`),
  CONSTRAINT `FK_sender_users` FOREIGN KEY (`sender`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table virtual_wallet.transactions: ~0 rows (approximately)
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;

-- Dumping structure for table virtual_wallet.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `password` varchar(68) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `card_id` int(11) NOT NULL,
  `photo` longblob NOT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT 1,
  UNIQUE KEY `uesrs_email_uindex` (`email`),
  UNIQUE KEY `uesrs_phone_number_uindex` (`phone_number`),
  UNIQUE KEY `uesrs_username_uindex` (`username`),
  KEY `FK_users_card` (`card_id`),
  CONSTRAINT `FK_users_card` FOREIGN KEY (`card_id`) REFERENCES `card` (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table virtual_wallet.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table virtual_wallet.wallet
DROP TABLE IF EXISTS `wallet`;
CREATE TABLE IF NOT EXISTS `wallet` (
  `wallet_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `amount` double DEFAULT NULL,
  PRIMARY KEY (`wallet_id`),
  KEY `FK_wallet_users` (`username`),
  CONSTRAINT `FK_wallet_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table virtual_wallet.wallet: ~0 rows (approximately)
/*!40000 ALTER TABLE `wallet` DISABLE KEYS */;
/*!40000 ALTER TABLE `wallet` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
