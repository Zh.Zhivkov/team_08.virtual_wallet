# Virtual Wallet

Here you will find information regarding the Virtual Wallet Assignment of Team 8.

This is the link for our Trello: https://trello.com/b/qHYSFZdq/virtual-wallet

## Project description

Our goal is to design and implement a Restful Web Application for people who want an easy way to send and receive money to their close ones.

## Functional details

Virtual Wallet enables users to manage and personalize their accounts. 
They can add cards and create wallets, withdraw funds and use those funds to send to anyone they want.
They can also manage their transactions and filter/sort them by whatever they want.
Admins can manage and filter users, also view all the transactions that were made.


## General Information:

Each user has **name**, **username**, **phone number**, **email** and **profile picture** (optional).

Each card has **card number**, **cardholder name**, **csv** and **expiration date**.

Each wallet has **name**, **balance** and can be **default** for its user.

Each transaction contains information regarding the users, amount and date included.

Application provides functionality based on the role of the user that is using it.
There are three types of users: `[GUEST]`, `[USER]` and `[ADMIN]`.

`[GUEST]` can view the landing page, register page and login page.

`[USER]` can add cards, create wallets, make transactions, manage their wallets, view their transactions.

`[ADMINS]` can manage all users and transactions.
