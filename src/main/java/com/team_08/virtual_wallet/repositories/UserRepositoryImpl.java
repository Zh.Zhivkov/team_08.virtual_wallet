package com.team_08.virtual_wallet.repositories;

import com.team_08.virtual_wallet.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    public static final String DOES_NOT_EXIST_FORMAT = "User with %s:%s does not exist";

    private final SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
            return user;
        }
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where enabled = 1", User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAllUsers(String[] filterDetails) {
        try(Session session = sessionFactory.openSession()) {
            Query<User> userQuery = buildQuery(filterDetails, session);
            return userQuery.list();
        }
    }


    @Override
    public User getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            return user;
        }
    }

    @Override
    public User getUserByName(String username) {

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(
                        String.format(DOES_NOT_EXIST_FORMAT,"username", username));
            }
            return query.getSingleResult();
        }
    }

    @Override
    public User getUserByPhoneNumber(String phoneNumber) {

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where phoneNumber = :phone_number", User.class);
            query.setParameter("phone_number", phoneNumber);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(
                        String.format(DOES_NOT_EXIST_FORMAT,"phone Number", phoneNumber));
            }
            return query.getSingleResult();
        }
    }

    @Override
    public User getUserByEmail(String email) {

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(
                        String.format(DOES_NOT_EXIST_FORMAT,"email", email));
            }
            return query.getSingleResult();
        }
    }

    @Override
    public boolean checkIfUserExist(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            return user != null;
        }
    }

    @Override
    public boolean checkIfUserExistByUserName(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            return !query.list().isEmpty();
        }
    }

    @Override
    public void constructUser(User user) {
        try(Session session = sessionFactory.openSession()) {

            session.beginTransaction();

            Query userQuery = session.createQuery(
                    "update User as u set " +
                            "phoneNumber = :phone_number," +
                            "email = :email where username = :username"
            );

            userQuery.setParameter("phone_number", user.getPhoneNumber());
            userQuery.setParameter("email", user.getEmail());
            userQuery.setParameter("username", user.getUsername());

            userQuery.executeUpdate();

            session.getTransaction().commit();
        }
    }

    @Override
    public User update(User user) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public void delete(User user) {
        user.setEnabled(0);
        try (Session session = sessionFactory.openSession()) {

            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    private Query<User> buildQuery(String[] filterDetails, Session session) {
        StringBuffer queryString = new StringBuffer("from User where enabled = :enabled");

        String username = filterDetails[0];
        String email = filterDetails[1];
        String phoneNumber = filterDetails[2];

        String sortBy = filterDetails[3];

        if(username != null) {
            queryString.append("and username = :username");
        }

        if(email != null) {
            queryString.append("and email = :email");
        }

        if(phoneNumber != null) {
            queryString.append("and phoneNumber = :phone_number");
        }

        if(sortBy != null) {
            switch (sortBy) {
                case "username": {
                    queryString.append("order by username");
                    break;
                }
                case "email": {
                    queryString.append("order by email");
                    break;
                }
                case "phone_number": {
                    queryString.append("order by phoneNumber");
                    break;
                }
            }
        }

        Query<User> userQuery = session.createQuery(String.valueOf(queryString), User.class);
        userQuery.setParameter("enabled",false);

        if (username != null) {
            userQuery.setParameter("username", username);
        }

        if (email != null) {
            userQuery.setParameter("email", email);
        }

        if (phoneNumber != null) {
            userQuery.setParameter("phone_number", phoneNumber);
        }

        return userQuery;
    }

}
