package com.team_08.virtual_wallet.repositories;

import com.team_08.virtual_wallet.models.User;

import java.util.List;

public interface UserRepository {

    User createUser(User user);

    List<User> getAllUsers();

    List<User> getAllUsers(String[] filterDetails);

    User getUserById(int id);

    User getUserByName(String userName);

    boolean checkIfUserExist(int id);

    boolean checkIfUserExistByUserName(String username);

    void constructUser(User user);

    User getUserByPhoneNumber(String phoneNumber);

    User getUserByEmail(String email);

    User update(User user);

    void delete(User user);
}
