package com.team_08.virtual_wallet;

import com.team_08.virtual_wallet.models.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableSwagger2
public class VirtualWalletApplication {

    public static void main(String[] args) {
        SpringApplication.run(VirtualWalletApplication.class, args);

    }

    @Bean
    public Docket swaggerConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.team_08.virtual_wallet"))
                .build()
                .apiInfo(apiDetails());

    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "Virtual Wallet API",
                "Virtual Wallet API, final project",
                "1.0",
                "Free to use",
                new springfox.documentation.service.Contact
                        ("Zhivko Zhivkov", "http://virtual_wallet.team_08.com", "zhivko@gmail.com"),
                "API License",
                "http://virtual_wallet.team_08.com",
                Collections.emptyList());


    }



}
