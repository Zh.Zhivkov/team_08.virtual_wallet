package com.team_08.virtual_wallet.controllers;

import com.team_08.virtual_wallet.models.User;
import com.team_08.virtual_wallet.models.dtos.DtoMapper;
import com.team_08.virtual_wallet.models.dtos.UserDto;
import com.team_08.virtual_wallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {

    private UserDetailsManager userDetailsManager;
    private UserService userService;
    private DtoMapper dtoMapper;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, UserService userService, DtoMapper dtoMapper, PasswordEncoder passwordEncoder) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
        this.dtoMapper = dtoMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "new-register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserDto userDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can't be empty!");
            return "register";
        }

        if(userDetailsManager.userExists(userDto.getUsername())) {
            model.addAttribute("error", "User with the same username already exists!");
            return "register";
        }

        User user = dtoMapper.fromUserDto(userDto);

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getUsername(),
                        passwordEncoder.encode(userDto.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);

        userService.constructUser(user);

        return "register-confirmation";
    }

    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }
}
