package com.team_08.virtual_wallet.services;

import com.team_08.virtual_wallet.models.User;

import java.util.List;

public interface UserService {

    User createUser(User user);

    List<User> getAllUsers();

    List<User> getAllUsers(String[] filterDetails);

    User getUserById(int id);

    User getUserByName(String userName);

    User getUserByPhoneNumber(String phoneNumber);

    User getUserByEmail(String email);

    boolean checkIfUserExistByUserName(String username);

    void constructUser(User user);

    void blockUser(User user);

    void unblockUser(User user);

    User update(User user);

    void delete(User user);
}
