package com.team_08.virtual_wallet.services;

import com.team_08.virtual_wallet.exceptions.DuplicateEntityException;
import com.team_08.virtual_wallet.exceptions.EntityNotFoundException;
import com.team_08.virtual_wallet.models.User;
import com.team_08.virtual_wallet.models.dtos.DtoMapper;
import com.team_08.virtual_wallet.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    public static final String USER_DOES_NOT_EXIST = "User with %s: %s does not exist";
    private final UserRepository userRepository;
    private final DtoMapper mapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, DtoMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }


    @Override
    public User createUser(User user) {
        if(checkIfUserExistByUserName(user.getUsername())){
            throw new DuplicateEntityException(String.format("User with username '%s' already exists!", user.getUsername()));
        }

        return userRepository.createUser(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public List<User> getAllUsers(String[] filterDetails) {
        return userRepository.getAllUsers(filterDetails);
    }

//    ToDo new method for throwing entity private not found,because of reusing
//    Todo Is getUserById needed when we use username for id?
    @Override
    public User getUserById(int id) {
    User user = userRepository.getUserById(id);
//        if (user == null) {
//            throw new EntityNotFoundException(String.format(USER_DOES_NOT_EXIST,"id",String.valueOf(id)));
//        }
        throwIfUserDoesNotExist(user,"id",String.valueOf(id));
        return user;
    }

    @Override
    public User getUserByName(String userName) {
        User user = userRepository.getUserByName(userName);
        if (user == null) {
            throw new EntityNotFoundException(String.format(USER_DOES_NOT_EXIST,"username",userName));
        }
        return user;
    }

    @Override
    public User getUserByPhoneNumber(String phoneNumber) {
        User user = userRepository.getUserByPhoneNumber(phoneNumber);
        if (user == null) {
            throw new EntityNotFoundException(String.format(USER_DOES_NOT_EXIST,"phone number",phoneNumber));
        }
        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        User user = userRepository.getUserByEmail(email);
        if (user == null) {
            throw new EntityNotFoundException(String.format(USER_DOES_NOT_EXIST,"email",email));
        }
        return user;
    }


    @Override
    public boolean checkIfUserExistByUserName(String username) {
        return userRepository.checkIfUserExistByUserName(username);
    }

    @Override
    public void constructUser(User user) {
        userRepository.constructUser(user);
    }
//    Todo is throwIfUserDoesNotExist needed here?
    @Override
    public void blockUser(User user){
        throwIfUserDoesNotExist(user,"username",user.getUsername());
        userRepository.getUserByName(user.getUsername());
        user.setBlocked(0);
        userRepository.update(user);
    }

    @Override
    public void unblockUser(User user){
        throwIfUserDoesNotExist(user,"username",user.getUsername());
        userRepository.getUserByName(user.getUsername());
        user.setBlocked(1);
        userRepository.update(user);
    }

    @Override
    public User update(User user) {
        throwIfUserDoesNotExist(user,"username",user.getUsername());
        userRepository.getUserByName(user.getUsername());
        userRepository.update(user);
        return user;
    }

    @Override
    public void delete(User user) {
        throwIfUserDoesNotExist(user,"username",user.getUsername());
        userRepository.delete(user);
    }

    private void throwIfUserDoesNotExist(User user,String property,String value){
        if (user == null) {
            throw new EntityNotFoundException(String.format(USER_DOES_NOT_EXIST,property,value));
        }
    }
}
