(function () {
    $('#cards_table tbody').on('mouseover', 'tr', function () {

        let card_id = $(this).find("td:first-child").html();

        $('#default_wallet_form').attr("action", "my-wallets/make-default/" + card_id);
        $(this).find('#wallet_transaction_id').attr("href", "/my-transactions?wallet_id=" + card_id);
        $('#delete_wallet_form').attr("action", "my-cards/delete-card/" + card_id);
    });
})();