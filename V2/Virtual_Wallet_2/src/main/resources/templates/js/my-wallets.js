(function () {
    $('#wallet_table tbody').on('mouseover', 'tr', function () {

        let wallet_id = $(this).find("td:first-child").html();

        $('#default_wallet_form').attr("action", "my-wallets/make-default/" + wallet_id);
        $(this).find('#wallet_transaction_id').attr("href", "/my-transactions?wallet_id=" + wallet_id);
        $('#delete_wallet_form').attr("action", "my-wallets/delete-wallet/" + wallet_id);
    });
})();