$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});

$("#open-profile").on("click", function () {
    $(".user-profile-area").removeClass("hidden");
    $(".edit-user-area").addClass("hidden");
    $(".change-pass-area").addClass("hidden");

    $("#open-profile").addClass("selected-tab");
    $("#edit-profile").removeClass("selected-tab");
    $("#reset-profile-pass").removeClass("selected-tab");
});

$("#edit-profile").on("click", function () {
    $(".user-profile-area").addClass("hidden");
    $(".edit-user-area").removeClass("hidden");
    $(".change-pass-area").addClass("hidden");

    $("#open-profile").removeClass("selected-tab");
    $("#edit-profile").addClass("selected-tab");
    $("#reset-profile-pass").removeClass("selected-tab");
});

$("#reset-profile-pass").on("click", function () {
    $(".user-profile-area").addClass("hidden");
    $(".edit-user-area").addClass("hidden");
    $(".change-pass-area").removeClass("hidden");

    $("#open-profile").removeClass("selected-tab");
    $("#edit-profile").removeClass("selected-tab");
    $("#reset-profile-pass").addClass("selected-tab");
});

$("#sidebarCollapse").on("click", function () {
    const element = $("#collapse-icon");
    if (!element.hasClass("fa-rotate-90")) {
        element.addClass("fa-rotate-90");
    } else {
        element.removeClass("fa-rotate-90");
    }
});