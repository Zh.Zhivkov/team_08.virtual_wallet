const validationRegex = {
  usernameLength: /^[^\>]{4,20}$/,
  usernameSymbols: /^[a-zA-Z0-9\.\_]*$/,
  passwordLength:/^[^\>]{8,}$/,
  // passwordSymbols: /^(?=.*[A-Z])(?=.*\d)(?=.*[a-z])[A-Za-z\d&]*$/, // Minimum eight characters, at least one lowercase letter, one number and one uppercase letter:
  passwordSymbols: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/, // Minimum eight characters, at least one lowercase letter, one number and one uppercase letter:
  email: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
  phone: /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
};

const usernameLengthError = "Username must be between 4 and 20 symbols long.";
const usernameSymbolsError = "Only letters, digits, dots and underscores allowed.";
const passwordLengthError = "Password must be at least 8 symbols long.";
const passwordSymbolsError = "Password must contain at least one lowercase letter, one uppercase letter and a digit.";
const emailError = "Please enter a valid email address.";
const phoneError = "Please enter a valid phone number.";
const passwordsDontMatchError = "Passwords don't match.";
const errorMessage = "Invalid field";

// const name = document.getElementById('name');
// name.addEventListener('blur', () => {
//   checkValid(name, validationRegex.name, errorMessage);
// });

var conditions = [];
for (var i = 0; i < 5; i++) {
  conditions.push(false);
}

const username = document.getElementById('username');
username.addEventListener('keypress', () => {
  checkValid2(username, validationRegex.usernameLength, validationRegex.usernameSymbols, usernameLengthError, usernameSymbolsError, 0);
  checkAllTrue(conditions, submitButton);
});
username.addEventListener('keyup', () => {
  checkValid2(username, validationRegex.usernameLength, validationRegex.usernameSymbols, usernameLengthError, usernameSymbolsError, 0);
  checkAllTrue(conditions, submitButton);
});
username.addEventListener('blur', () => {
  checkValid2(username, validationRegex.usernameLength, validationRegex.usernameSymbols, usernameLengthError, usernameSymbolsError, 0);
  checkAllTrue(conditions, submitButton);
});

const password = document.getElementById('password');
password.addEventListener('keypress', () => {
  checkValid2(password, validationRegex.passwordSymbols, validationRegex.passwordLength, passwordSymbolsError, passwordLengthError, 1);
  checkAllTrue(conditions, submitButton);
});
password.addEventListener('keyup', () => {
  checkValid2(password, validationRegex.passwordSymbols, validationRegex.passwordLength, passwordSymbolsError, passwordLengthError, 1);
  checkAllTrue(conditions, submitButton);
});
password.addEventListener('blur', () => {
  checkValid2(password, validationRegex.passwordSymbols, validationRegex.passwordLength, passwordSymbolsError, passwordLengthError, 1);
  checkAllTrue(conditions, submitButton);
});

const passwordConfirmation = document.getElementById("password_confirmation");
passwordConfirmation.addEventListener('keypress', () => {
  passwordsMatch(passwordConfirmation, password, passwordConfirmation, passwordsDontMatchError, 2);
  checkAllTrue(conditions, submitButton);
});
passwordConfirmation.addEventListener('keyup', () => {
  passwordsMatch(passwordConfirmation, password, passwordConfirmation, passwordsDontMatchError, 2);
  checkAllTrue(conditions, submitButton);
});
passwordConfirmation.addEventListener('blur', () => {
  passwordsMatch(passwordConfirmation, password, passwordConfirmation, passwordsDontMatchError, 2);
  checkAllTrue(conditions, submitButton);
});

const email = document.getElementById('email');
email.addEventListener('keypress', () => {
  checkValid(email, validationRegex.email, emailError, 3);
  checkAllTrue(conditions, submitButton);
});
email.addEventListener('keyup', () => {
  checkValid(email, validationRegex.email, emailError, 3);
  checkAllTrue(conditions, submitButton);
});
email.addEventListener('blur', () => {
  checkValid(email, validationRegex.email, emailError, 3);
  checkAllTrue(conditions, submitButton);
});

const phone = document.getElementById('phone_number');
phone.addEventListener('keypress', () => {
  checkValid(phone, validationRegex.phone, phoneError, 4);
  checkAllTrue(conditions, submitButton);
});
phone.addEventListener('keyup', () => {
  checkValid(phone, validationRegex.phone, phoneError, 4);
  checkAllTrue(conditions, submitButton);
});
phone.addEventListener('blur', () => {
  checkValid(phone, validationRegex.phone, phoneError, 4);
  checkAllTrue(conditions, submitButton);
});

const submitButton = document.getElementById("submit_button");


function checkValid(input, regex, errorMessage, conditionNumber) {
  const inputValue = input.value;
  if (inputValue !== "") {
    if (!regex.test(inputValue)) {
      conditions[conditionNumber] = false;
      showError(input, errorMessage);
    } else {
      conditions[conditionNumber] = true;
      showSuccess(input);
    }
  } else {
    conditions[conditionNumber] = false;
    removeClassesIfTheyExist(input);
  }
}

function checkValid2(input, regex1, regex2, errorMessage1, errorMessage2, conditionNumber) {
  const inputValue = input.value;
  if (inputValue !== "") {
    if (!regex1.test(inputValue)) {
      conditions[conditionNumber] = false;
      showError(input, errorMessage1);
    } else if (!regex2.test(inputValue)) {
      conditions[conditionNumber] = false;
      showError(input, errorMessage2)
    } else {
      conditions[conditionNumber] = true;
      showSuccess(input);
    }
  } else {
    conditions[conditionNumber] = false;
    removeClassesIfTheyExist(input);
  }
}

function passwordsMatch(input, password, confirmation, errorMessage, conditionNumber) {
  const inputValue = input.value;
  if (inputValue !== "") {
    if (!(password.value === confirmation.value)) {
      conditions[conditionNumber] = false;
      showError(input, errorMessage);
    } else {
      conditions[conditionNumber] = true;
      showSuccess(input);
    }
  } else {
    conditions[conditionNumber] = false;
    removeClassesIfTheyExist(input);
  }
}

function checkAllTrue(conditions, button) {
  if(conditions[0] && conditions[1] && conditions[2] && conditions[3] && conditions[4]) {
    button.disabled = false;
  } else {
    button.disabled = true;
  }
}

function showError(input, errorMessage) {
  const formControl = input.parentElement;
  formControl.classList.add("error");
  var small = formControl.querySelector('p');
  small.innerText = errorMessage;
}

function showSuccess(input) {
  const formControl = input.parentElement;
  var small = formControl.querySelector('p');
  small.innerText = "";
  formControl.classList.remove("error");
  formControl.classList.add("success");
}

function removeClassesIfTheyExist(input) {
  const formControl = input.parentElement;
  formControl.classList.remove("error");
  formControl.classList.remove("success");
}