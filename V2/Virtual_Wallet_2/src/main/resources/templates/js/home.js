$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});

$("#open-profile").on("click", function () {
    $(".user-profile-area").removeClass("hidden");
    $(".edit-user-area").addClass("hidden");
    $(".change-pass-area").addClass("hidden");
});

$("#edit-profile").on("click", function () {
    $(".user-profile-area").addClass("hidden");
    $(".edit-user-area").removeClass("hidden");
    $(".change-pass-area").addClass("hidden");
});

$("#reset-profile-pass").on("click", function () {
    $(".user-profile-area").addClass("hidden");
    $(".edit-user-area").addClass("hidden");
    $(".change-pass-area").removeClass("hidden");
});

$("#sidebarCollapse").on("click", function () {
    const element = $("#collapse-icon");
    if (!element.hasClass("fa-rotate-90")) {
        element.addClass("fa-rotate-90");
    } else {
        element.removeClass("fa-rotate-90");
    }
});

const validationRegex = {
    name: /[a-z]{3,}/,
    username: /^\w{6,20}$/,
    password: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, // Minimum eight characters, at least one letter, one number and one special character:
    email: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
    phone: /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
}

const errorMessage = "Invalid field";

const name = document.getElementById('reg-first-name');
name.addEventListener('blur', () => {
    checkValid(name, validationRegex.name, errorMessage);
});

const username = document.getElementById('reg-username');
username.addEventListener('blur', () => {
    checkValid(username, validationRegex.username, errorMessage);
});

const password = document.getElementById('reg-user-password');
password.addEventListener('blur', () => {
    checkValid(password, validationRegex.password, errorMessage);
});

const email = document.getElementById('reg-user-email');
email.addEventListener('blur', () => {
    checkValid(email, validationRegex.email, errorMessage);
});

const phone = document.getElementById('reg-phone');
phone.addEventListener('blur', () => {
    checkValid(phone, validationRegex.phone, errorMessage);
});

function checkValid(input, regex, errorMessage) {
    const inputValue = input.value;
    if (inputValue !== "") {
        if (!regex.test(inputValue)) {
            showError(input, errorMessage);
        } else {
            showSuccess(input);
        }
    } else {
        removeClassesIfTheyExist(input);
    }
}

function showError(input, errorMessage) {
    const formControl = input.parentElement;
    formControl.classList.add("error");
    const small = formControl.querySelector('small');
    small.innerText = errorMessage;
}

function showSuccess(input) {
    const formControl = input.parentElement;
    formControl.classList.remove("error");
    formControl.classList.add("success");
}

function removeClassesIfTheyExist(input) {
    const formControl = input.parentElement;
    formControl.classList.remove("error");
    formControl.classList.remove("success");
}