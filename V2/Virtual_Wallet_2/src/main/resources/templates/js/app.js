$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});

$("#open-profile").on("click", function () {
    $(".user-profile-area").removeClass("hidden");
    $(".edit-user-area").addClass("hidden");
    $(".change-pass-area").addClass("hidden");

    $("#open-profile").addClass("selected-tab");
    $("#edit-profile").removeClass("selected-tab");
    $("#reset-profile-pass").removeClass("selected-tab");
});

$("#edit-profile").on("click", function () {
    $(".user-profile-area").addClass("hidden");
    $(".edit-user-area").removeClass("hidden");
    $(".change-pass-area").addClass("hidden");

    $("#open-profile").removeClass("selected-tab");
    $("#edit-profile").addClass("selected-tab");
    $("#reset-profile-pass").removeClass("selected-tab");
});

$("#reset-profile-pass").on("click", function () {
    $(".user-profile-area").addClass("hidden");
    $(".edit-user-area").addClass("hidden");
    $(".change-pass-area").removeClass("hidden");

    $("#open-profile").removeClass("selected-tab");
    $("#edit-profile").removeClass("selected-tab");
    $("#reset-profile-pass").addClass("selected-tab");
});

$("#sidebarCollapse").on("click", function () {
    const element = $("#collapse-icon");
    if (!element.hasClass("fa-rotate-90")) {
        element.addClass("fa-rotate-90");
    } else {
        element.removeClass("fa-rotate-90");
    }
});

$("#getUsersSection").on("click", function () {
    $(".users-list-section").removeClass("hidden");
    $(".deleted-users-section").addClass("hidden");
    $(".suspended-users-section").addClass("hidden");
});

$("#suspendedUsersSection").on("click", function () {
    $(".suspended-users-section").removeClass("hidden");
    $(".deleted-users-section").addClass("hidden");
    $(".users-list-section").addClass("hidden");
});

$("#deletedUsersSection").on("click", function () {
    $(".deleted-users-section").removeClass("hidden");
    $(".suspended-users-section").addClass("hidden");
    $(".users-list-section").addClass("hidden");
});


(function () {
    var dataContainer = $('#transaction-template')
    var container = $('#pagination');
    var json = $('#users_json').text();
    var dataArray = JSON.parse(json);

    container.pagination({
        dataSource: dataArray,
        pageSize: 5,
        showPrevious: true,
        showNext: true,
        showPageNumbers: true,
        showGoInput: true,
        showGoButton: true,
        callback: function (response, pagination) {
            var dataHtml = "";
            $.each(response, function (index, item) {
                var template = document.getElementById("transaction-template-row");
                template.content.querySelector('#firstName').textContent = item.firstName;
                template.content.querySelector('#lastName').textContent = item.lastName;
                template.content.querySelector('#username').textContent = item.username;
                template.content.querySelector('#email').textContent = item.email;
                template.content.querySelector('#phone').textContent = item.phoneNumber;
                template.content.querySelector('#id').textContent = item.id;
                if (item.blocked) {
                    template.content.querySelector('#status').textContent = "Blocked";
                } else {
                    template.content.querySelector('#status').textContent = "Active";
                }

                dataHtml += template.innerHTML;
            });

            dataContainer.html(dataHtml);
        }
    });

    var jsonBlocked = $('#blocked_users_json').text();
    var dataArrayBlocked = JSON.parse(jsonBlocked);
    var dataContainerBlocked = $('#transaction-template-blocked');
    var containerBlocked = $('#pagination-blocked');

    containerBlocked.pagination({
        dataSource: dataArrayBlocked,
        pageSize: 5,
        showPrevious: true,
        showNext: true,
        showPageNumbers: true,
        showGoInput: true,
        showGoButton: true,
        callback: function (response, pagination) {
            var dataHtml = "";
            $.each(response, function (index, item) {
                if(item.blocked) {
                    var template = document.getElementById("transaction-template-row-blocked");
                    template.content.querySelector('#firstName-blocked').textContent = item.firstName;
                    template.content.querySelector('#lastName-blocked').textContent = item.lastName;
                    template.content.querySelector('#username-blocked').textContent = item.username;
                    template.content.querySelector('#email-blocked').textContent = item.email;
                    template.content.querySelector('#phone-blocked').textContent = item.phoneNumber;
                    template.content.querySelector('#id-blocked').textContent = item.id;

                    if (item.blocked) {
                        template.content.querySelector('#status-blocked').textContent = "Blocked";
                    } else {
                        template.content.querySelector('#status-blocked').textContent = "Active";
                    }

                    dataHtml += template.innerHTML;
                }
            });

            dataContainerBlocked.html(dataHtml);
        }
    });

    var jsonDisabled = $('#disabled_users_json').text();
    var dataArrayDisabled = JSON.parse(jsonDisabled);
    var dataContainerDisabled = $('#transaction-template-disabled');
    var containerDisabled = $('#pagination-disabled');

    containerDisabled.pagination({
        dataSource: dataArrayDisabled,
        pageSize: 5,
        showPrevious: true,
        showNext: true,
        showPageNumbers: true,
        showGoInput: true,
        showGoButton: true,
        callback: function (response, pagination) {
            var dataHtml = "";
            $.each(response, function (index, item) {
                if(!item.enabled) {
                    var template = document.getElementById("transaction-template-row-disabled");
                    template.content.querySelector('#firstName-disabled').textContent = item.firstName;
                    template.content.querySelector('#lastName-disabled').textContent = item.lastName;
                    template.content.querySelector('#username-disabled').textContent = item.username;
                    template.content.querySelector('#email-disabled').textContent = item.email;
                    template.content.querySelector('#phone-disabled').textContent = item.phoneNumber;
                    template.content.querySelector('#id-disabled').textContent = item.id;
                    if (item.blocked) {
                        template.content.querySelector('#status-disabled').textContent = "Disabled";
                    } else {
                        template.content.querySelector('#status-disabled').textContent = "Disabled";
                    }

                    dataHtml += template.innerHTML;
                }
            });

            dataContainerDisabled.html(dataHtml);
        }
    })
})();

(function () {
    $('#user_table tbody').on('mouseover', 'tr', function () {
        let user_id = $(this).find("td:last-child").html();

        var transaction_page_id = "transactions?sender=" + user_id;

        $(this).find("#user_transactions").attr("href", transaction_page_id);

        $('#user_transactions').attr("href", transaction_page_id);
        $('#block_form').attr("action", "users/block/" + user_id);
        $('#disable_form').attr("action", "users/disable/" + user_id);
        $('#activation_form').attr("action", "users/activate/" + user_id);
    });

    $('#user_table_blocked tbody').on('mouseover', 'tr', function () {
        let user_id = $(this).find("td:last-child").html();

        var transaction_page_id = "transactions?sender=" + user_id;

        $(this).find("#user_transactions_blocked").attr("href", transaction_page_id);

        $('#user_transactions_blocked').attr("href", transaction_page_id);

        $('#block_form').attr("action", "users/block/" + user_id);
        $('#disable_form').attr("action", "users/disable/" + user_id);
        $('#activation_form').attr("action", "users/activate/" + user_id);
    });

    $('#user_table_disabled tbody').on('mouseover', 'tr', function () {
        let user_id = $(this).find("td:last-child").html();

        var transaction_page_id = "transactions?sender=" + user_id;

        $(this).find("#user_transactions_disabled").attr("href", transaction_page_id);

        $('#user_transactions_disabled').attr("href", transaction_page_id);

        $('#block_form').attr("action", "users/block/" + user_id);
        $('#disable_form').attr("action", "users/disable/" + user_id);
        $('#activation_form').attr("action", "users/activate/" + user_id);
    })
})();