$(document).ready(function () {

    $('#sender').select2({
        placeholder: 'Select sender',
        allowClear: true,
        minimumInputLength: 4,
        minimumResultsForSearch: 1,
        ajax: {
            url: "http://localhost:8080/api/users",
            dataType: "json",
            type: "GET",
            crossDomain: true,
            data: function (params) {
                return {
                    username: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.username,
                            id: item.id,
                            photo: item.photo,
                            firstName: item.firstName,
                            lastName: item.lastName
                        }
                    })
                };
            }
        },
        templateResult: formatState,
        templateSelection: formatState
    });

    $('#recipient').select2({
        placeholder: 'Select recipient',
        allowClear: true,
        minimumInputLength: 4,
        minimumResultsForSearch: 1,
        ajax: {
            url: "http://localhost:8080/api/users",
            dataType: "json",
            type: "GET",
            crossDomain: true,
            data: function (params) {
                return {
                    username: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.username,
                            id: item.id,
                            photo: item.photo,
                            firstName: item.firstName,
                            lastName: item.lastName
                        }
                    })
                };
            }
        },
        templateResult: formatState,
        templateSelection: formatState
    });


    function formatState (state) {
        if (!state.id) {
            return state.text;
        }

        var $state = $(
            '<span><img class="img-flag" /> <span></span></span>'
        );

        // Use .text() instead of HTML string concatenation to avoid script injection issues
        $state.find("span").text(state.firstName + " " + state.lastName + ", username: " + state.text);
        $state.find("img").attr("src", "data:image/png;base64, " + state.photo);
        $state.find("img").attr("width", "20px");

        return $state;
    }


});

(function () {
    var dataContainer = $('#transaction-template-tr')
    var container = $('#pagination-tr');
    var json = $('#transactions_json').text();
    var dataArray = JSON.parse(json);

    container.pagination({
        dataSource: dataArray,
        pageSize: 5,
        showPrevious: true,
        showNext: true,
        showPageNumbers: true,
        showGoInput: true,
        showGoButton: true,
        callback: function (response, pagination) {
            var dataHtml = "";
            $.each(response, function (index, item) {
                var template = document.getElementById("transaction-template-row-tr");
                template.content.querySelector('#transaction_id').textContent = item.id;
                template.content.querySelector('#transaction_sender').textContent = item.sender;
                template.content.querySelector('#transaction_recipient').textContent = item.recipient;
                template.content.querySelector('#transaction_amount').textContent = item.amount;
                template.content.querySelector('#transaction_status').textContent = item.status;
                template.content.querySelector('#transaction_date').textContent = item.date;

                dataHtml += template.innerHTML;
            });

            dataContainer.html(dataHtml);
        }
    });
})();