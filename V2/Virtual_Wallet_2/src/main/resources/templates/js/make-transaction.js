// window.onload = function() {
//     if (window.jQuery) {
//         // jQuery is loaded
//         alert("Yeah!");
//     } else {
//         // jQuery is not loaded
//         alert("Doesn't Work");
//     }
// };

$(document).ready(function() {
    $('#select_by').on("change", function () {
        var selectIndex = $('#select_by')[0].selectedIndex;
        var filter;
        if(selectIndex === 1) {
            $('#select_recipient').select2({
                placeholder: 'Select recipient',
                allowClear: true,
                minimumInputLength: 4,
                minimumResultsForSearch: 1,
                ajax: {
                    url: "http://localhost:8080/api/users",
                    dataType: "json",
                    type: "GET",
                    crossDomain: true,
                    data: function (params) {
                        return {
                            username: params.term,
                            enabled: true
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.username,
                                    id: item.id,
                                    photo: item.photo,
                                    firstName: item.firstName,
                                    lastName: item.lastName
                                }
                            })
                        };
                    }
                },
                templateResult: formatState,
                templateSelection:formatState
            });


            function formatState (state) {
                if (!state.id) {
                    return state.text;
                }

                var $state = $(
                    '<span><img class="img-flag" /> <span></span></span>'
                );

                // Use .text() instead of HTML string concatenation to avoid script injection issues
                $state.find("span").text(state.firstName + " " + state.lastName + ", username: " + state.text);
                $state.find("img").attr("src", "data:image/png;base64, " + state.photo);
                $state.find("img").attr("width", "20px");

                return $state;
            }
        } else if (selectIndex === 2) {
            $('#select_recipient').select2({
                placeholder: 'Select recipient',
                allowClear: true,
                minimumInputLength: 3,
                minimumResultsForSearch: 1,
                ajax: {
                    url: "http://localhost:8080/api/users",
                    dataType: "json",
                    type: "GET",
                    crossDomain: true,
                    data: function (params) {
                        return {
                            phoneNumber: params.term,
                            enabled: true
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.phoneNumber,
                                    id: item.id,
                                    photo: item.photo,
                                    firstName: item.firstName,
                                    lastName: item.lastName
                                }
                            })
                        };
                    }
                },
                templateResult: formatState,
                templateSelection:formatState
            });


            function formatState (state) {
                if (!state.id) {
                    return state.text;
                }

                var $state = $(
                    '<span><img class="img-flag" /> <span></span></span>'
                );

                // Use .text() instead of HTML string concatenation to avoid script injection issues
                $state.find("span").text(state.firstName + " " + state.lastName + ", phone number: " + state.text);
                $state.find("img").attr("src", "data:image/png;base64, " + state.photo);
                $state.find("img").attr("width", "20px");

                return $state;
            }
        } else if (selectIndex === 3) {
            $('#select_recipient').select2({
                placeholder: 'Select recipient',
                allowClear: true,
                minimumInputLength: 2,
                minimumResultsForSearch: 1,
                ajax: {
                    url: "http://localhost:8080/api/users",
                    dataType: "json",
                    type: "GET",
                    crossDomain: true,
                    data: function (params) {
                        return {
                            email: params.term,
                            enabled: true
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.email,
                                    id: item.id,
                                    photo: item.photo,
                                    firstName: item.firstName,
                                    lastName: item.lastName
                                }
                            })
                        };
                    }
                },
                templateResult: formatState,
                templateSelection:formatState
            });


            function formatState (state) {
                if (!state.id) {
                    return state.text;
                }

                var $state = $(
                    '<span><img class="img-flag" /> <span></span></span>'
                );

                // Use .text() instead of HTML string concatenation to avoid script injection issues
                $state.find("span").text(state.firstName + " " + state.lastName + ", email: " + state.text);
                $state.find("img").attr("src", "data:image/png;base64, " + state.photo);
                $state.find("img").attr("width", "20px");

                return $state;
            }
        }


    });

    $('#select_wallet').on("change", function () {
        $('#balance_field').text($('option:selected', this).attr('name'));
    });

    $('#amount').on("keypress", function () {
        alert(this.text());
        if(this.text() > $('#balance_field').text()) {
            $('#amount').css("border-color", "orangered");
        } else {
            $('#amount').css("border-color", "white");
        }
    });

});

// $('select').select2({
    // tags: true,
    // multiple: true,
    // tokenSeparators: [',', ' '],
    // placeholder: 'Select user',
    // allowClear: true,
    // minimumInputLength: 2,
    // minimumResultsForSearch: 1
    // ajax: {
    //     url: "localhost:8080/api/users",
    //     dataType: "json",
    //     type: "GET",
    //     data: function (params) {
    //         return {
    //             username: params.term,
    //         };
    //     },
    //     processResults: function (data) {
    //         return {
    //             results: $.map(data, function (item) {
    //                 return {
    //                     text: item.username,
    //                     id: item.id
    //                 }
    //             })
    //         };
    //     }
    // }
// });

// $('select').select2({
//     // tags: true,
//     // multiple: true,
//     // tokenSeparators: [',', ' '],
//     placeholder: 'Select user',
//     minimumInputLength: 2,
//     minimumResultsForSearch: 1,
//     ajax: {
//         url: "localhost:8080/api/users",
//         dataType: "json",
//         type: "GET",
//         data: function (params) {
//             return {
//                 username: params.term,
//             };
//         },
//         processResults: function (data) {
//             return {
//                 results: $.map(data, function (item) {
//                     return {
//                         text: item.username,
//                         id: item.id
//                     }
//                 })
//             };
//         }
//     }
// });

// $('select').select2({
//     placeholder: 'Search for a repository',
//     minimumInputLength: 1,
//     ajax: {
//         url: "https://api.github.com/search/repositories",
//         dataType: 'json',
//         delay: 250,
//         data: function (params) {
//             return {
//                 q: params.term, // search term
//                 page: params.page
//             };
//         },
//         processResults: function (data, params) {
//             // parse the results into the format expected by Select2
//             // since we are using custom formatting functions we do not need to
//             // alter the remote JSON data, except to indicate that infinite
//             // scrolling can be used
//             params.page = params.page || 1;
//
//             return {
//                 results: data.items,
//                 pagination: {
//                     more: (params.page * 30) < data.total_count
//                 }
//             };
//         },
//         cache: true
//     },
//     templateResult: formatRepo,
//     templateSelection: formatRepoSelection
// });
//
// function formatRepo (repo) {
//     if (repo.loading) {
//         return repo.text;
//     }
//
//     var $container = $(
//         "<div class='select2-result-repository clearfix'>" +
//         "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
//         "<div class='select2-result-repository__meta'>" +
//         "<div class='select2-result-repository__title'></div>" +
//         "<div class='select2-result-repository__description'></div>" +
//         "<div class='select2-result-repository__statistics'>" +
//         "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> </div>" +
//         "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> </div>" +
//         "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> </div>" +
//         "</div>" +
//         "</div>" +
//         "</div>"
//     );
//
//     $container.find(".select2-result-repository__title").text(repo.full_name);
//     $container.find(".select2-result-repository__description").text(repo.description);
//     $container.find(".select2-result-repository__forks").append(repo.forks_count + " Forks");
//     $container.find(".select2-result-repository__stargazers").append(repo.stargazers_count + " Stars");
//     $container.find(".select2-result-repository__watchers").append(repo.watchers_count + " Watchers");
//
//     return $container;
// }
//
// function formatRepoSelection (repo) {
//     return repo.full_name || repo.text;
// }

// $(".js-example-data-ajax").select2({
//     ajax: {
//         url: "https://api.github.com/search/repositories",
//         dataType: 'json',
//         delay: 250,
//         data: function (params) {
//             return {
//                 q: params.term, // search term
//                 page: params.page
//             };
//         },
//         processResults: function (data, params) {
//             // parse the results into the format expected by Select2
//             // since we are using custom formatting functions we do not need to
//             // alter the remote JSON data, except to indicate that infinite
//             // scrolling can be used
//             params.page = params.page || 1;
//
//             return {
//                 results: data.items,
//                 pagination: {
//                     more: (params.page * 30) < data.total
//                 }
//             };
//         },
//         cache: true
//     },
//     placeholder: 'Search for a repository',
//     minimumInputLength: 1,
//     templateResult: formatRepo,
//     templateSelection: formatRepoSelection
// });
//
// function formatRepo (repo) {
//     if (repo.loading) {
//         return repo.text;
//     }
//
//     const $container = $(
//         "<div class='select2-result-repository clearfix'>" +
//         "<div class='select2-result-repository__avatar'><img src='" + "http://localhost:63342/virtual_wallet/virtual_wallet.main/templates/logo.png" + "' alt='rip'/></div>" +
//         "<div class='select2-result-repository__meta'>" +
//         "<div class='select2-result-repository__title'></div>" +
//         "<div class='select2-result-repository__description'></div>" +
//         "<div class='select2-result-repository__statistics'>" +
//         "</div>" +
//         "</div>" +
//         "</div>"
//     );
//
//     $container.find(".select2-result-repository__title").text(repo.name);
//     $container.find(".select2-result-repository__description").text(repo.description);
//
//     return $container;
// }
//
// function formatRepoSelection (repo) {
//     return repo.name || repo.text;
// }