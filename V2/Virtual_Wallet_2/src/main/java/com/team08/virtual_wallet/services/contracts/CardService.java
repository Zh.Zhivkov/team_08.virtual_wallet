package com.team08.virtual_wallet.services.contracts;

import com.team08.virtual_wallet.models.Card;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import com.team08.virtual_wallet.models.dto.CardEditDto;

import java.util.List;

public interface CardService {

    Card getCardById(int id);

    Card createCard(Card card, String username);

    List<Card> getAllCards(String username);

    boolean checkCardBelongingByCardNumber(String cardNumber, String username);

    boolean checkCardExistById(int id);

    boolean checkCardIsUnique(String cardNumber, String csv);

    Card findUserCardByCardNumberContaining(User user, String lastNumbers);

    Card editCard(int id, CardEditDto cardEditDto);

    void deleteCard(int id);

    List<Card> getAllCardsByUser(String name);
}
