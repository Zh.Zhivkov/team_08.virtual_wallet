package com.team08.virtual_wallet.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String message) {
        super(message);
    }

    public DuplicateEntityException(String itemType, String attribute, String value) {
        super(String.format("%s with %s: %s already exist.", itemType, attribute, value));
    }

    public DuplicateEntityException(String itemType,  String value) {
        super(String.format("%s with %s: %s already exist.", value));
    }
}
