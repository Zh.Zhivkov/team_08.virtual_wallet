package com.team08.virtual_wallet.repositories;

import com.team08.virtual_wallet.models.Card;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WalletRepository extends JpaRepository<Wallet,Integer> {

    List<Wallet> findAllByEnabledTrue();


    Optional<Wallet> findWalletById(int id);

    Wallet getWalletById(int id);

    Wallet getWalletByName(String walletName);

    List<Wallet> findAllByUserAndEnabledTrue(User user);


    List<Wallet> findAllByUserAndNameLike(User user, String name);

    List<Wallet> findAllByUserAndNameLikeAndEnabledTrueOrderByBalanceAsc(User user, String name);

    List<Wallet> findAllByUserAndNameLikeAndEnabledTrueOrderByBalanceDesc(User user, String name);

    List<Wallet> findAllByUserAndEnabledTrueOrderByBalanceDesc(User user);

    List<Wallet> findAllByUserAndEnabledTrueOrderByBalanceAsc(User user);

}
