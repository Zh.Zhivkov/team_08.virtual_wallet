package com.team08.virtual_wallet.repositories;

import com.sun.tools.javac.util.List;
import com.team08.virtual_wallet.models.Card;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {

    Card getCardById(int id);

    boolean existsCardById(int id);

    boolean existsCardByCardNumberAndCsv(String cardNumber, String csv);

    Card findByUserAndCardNumberLike(User user, String lastNumbers);

    Card getByCardNumberAndCsv(String cardNumber, String csv);

    List<Card> findAllByUserAndEnabledTrue(User user);
}
