package com.team08.virtual_wallet.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TransactionDisplayDto {

    private String id;
    private String sender;
    private String recipient;
    private String amount;
    private String status;
    private String date;
    private String direction;
}
