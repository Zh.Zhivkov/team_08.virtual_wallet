package com.team08.virtual_wallet.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@NoArgsConstructor @Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Card {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Pattern(regexp = "^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$", message = "Card number must be in xxxx-xxxx-xxxx-xxxx format.")
    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "expiration_date")
    private String expirationDate;

    @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "Card holder name can only contain letters")
    @Size(min = 2, max = 40, message = "Card holder name is not in permissible size")
    @Column(name = "cardholder_name")
    private String cardholderName;

    @Column(name = "csv")
    private String csv;

    @ManyToOne(targetEntity = User.class,fetch = FetchType.EAGER)
    private User user;

    @Column(name = "enabled")
    private boolean enabled;


    public Card(String cardNumber, String expirationDate, String cardholderName, String csv) {
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cardholderName = cardholderName;
        this.csv = csv;

    }
}
