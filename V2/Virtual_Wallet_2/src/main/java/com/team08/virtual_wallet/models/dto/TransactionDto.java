package com.team08.virtual_wallet.models.dto;

import com.team08.virtual_wallet.models.Wallet;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
@NoArgsConstructor
public class TransactionDto {
    @NotNull
    private int recipientId;

    @NotNull
    @Positive
    private double amount;

    @NotNull
    private int senderWalletId;
}
