package com.team08.virtual_wallet.controllers;

import com.team08.virtual_wallet.models.ConfirmationToken;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.repositories.ConfirmationTokenRepository;
import com.team08.virtual_wallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ConfirmationController {
    private UserService userService;
    private ConfirmationTokenRepository confirmationTokenRepository;

    public ConfirmationController() {
    }

    @Autowired
    public ConfirmationController(UserService userService, ConfirmationTokenRepository confirmationTokenRepository) {
        this.userService = userService;
        this.confirmationTokenRepository = confirmationTokenRepository;
    }

    @RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token")String confirmationToken)  {

        int a = 2 + 3;
        boolean tokenExists = confirmationTokenRepository.existsConfirmationTokenByTokenString(confirmationToken);
        ConfirmationToken token = confirmationTokenRepository.findByTokenString(confirmationToken);

        if(token != null) {
            User user = userService.getUserByEmail(token.getUser().getEmail());
            userService.verifyUser(user.getUsername());
            modelAndView.addObject("username", user.getUsername());
            modelAndView.setViewName("account-verified");
        }

        else {
            modelAndView.addObject("message","The link is invalid or broken!");
            modelAndView.setViewName("error");
        }

        return modelAndView;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ConfirmationTokenRepository getConfirmationTokenRepository() {
        return confirmationTokenRepository;
    }

    public void setConfirmationTokenRepository(ConfirmationTokenRepository confirmationTokenRepository) {
        this.confirmationTokenRepository = confirmationTokenRepository;
    }
}
