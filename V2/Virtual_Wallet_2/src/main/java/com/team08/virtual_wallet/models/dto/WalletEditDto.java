package com.team08.virtual_wallet.models.dto;

import com.team08.virtual_wallet.models.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data @NoArgsConstructor
public class WalletEditDto {

    int id;

    private double balance;

    private String name;

    private String oldNAme;

    private String userName;

    private boolean isDefault;

    private boolean enabled;
}
