package com.team08.virtual_wallet.models;

import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Data
public class BankTransfer  {

    private int amount;

    private String currency;

    private String description;

    private String idempotencyKey;

    private CardDetails cardDetails;

}
