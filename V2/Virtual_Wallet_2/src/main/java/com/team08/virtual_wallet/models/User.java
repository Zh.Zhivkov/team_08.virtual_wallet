package com.team08.virtual_wallet.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "users")
@Data
public class User {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "password")
    private String password;

    @Lob
    @Column(name = "profile_picture")
    private byte[] photo;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "enabled")
    private boolean enabled;

    @OneToMany(targetEntity = Card.class)
    private List<Card> cards;

    @OneToMany(targetEntity = GrantedAuthority.class)
    private List<GrantedAuthority> authorities;

    @OneToMany(targetEntity = Wallet.class)
    private List<Wallet> wallets;

    @Column(name = "blocked")
    private boolean blocked;

    @Column(name = "verified")
    private boolean verified;

    public User() {
        authorities = new ArrayList<>();
        wallets = new ArrayList<>();
        cards = new ArrayList<>();
    }

    @Override
    public String toString() {
        return username;
    }

}

