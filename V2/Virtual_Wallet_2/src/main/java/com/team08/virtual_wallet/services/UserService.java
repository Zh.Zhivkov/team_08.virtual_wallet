package com.team08.virtual_wallet.services;

import com.team08.virtual_wallet.exceptions.EntityDeletedException;
import com.team08.virtual_wallet.exceptions.EntityNotFoundException;
import com.team08.virtual_wallet.models.Card;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import com.team08.virtual_wallet.repositories.CardRepository;
import com.team08.virtual_wallet.repositories.UserRepository;
import com.team08.virtual_wallet.repositories.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final CardRepository cardRepository;
    private final WalletRepository walletRepository;

    @Autowired
    public UserService(UserRepository userRepository, CardRepository cardRepository, WalletRepository walletRepository) {
        this.userRepository = userRepository;
        this.cardRepository = cardRepository;
        this.walletRepository = walletRepository;
    }

    public List<User> getAllUsers(Pageable pageable) {
        List<User> users = new ArrayList<>();

        userRepository.findAllByEnabledTrue(pageable)
                .forEach(users::add);

        return users;
    }

    public List<User> getUsersByUsername(String username) {
        return userRepository.findAllByUsernameLike("%" + username + "%");
    }

    public List<User> getUsersByPhoneNumber(String phoneNumber) {
        return userRepository.findAllByPhoneNumberLike("%" + phoneNumber + "%");
    }

    public List<User> getUsersByEmail(String email) {
        return userRepository.findAllByEmailLike("%" + email + "%");
    }

    public User creteUser(User user) {

        user.setEnabled(true);
        userRepository.save(user);

        return user;
    }

    public User getUserById(int id) {

        User user = userRepository.getUserById(id);

        if (user == null) {
            throw new EntityNotFoundException("User", "id", String.valueOf(id));
        }

        if (user.isEnabled() == false) {
            throw new EntityDeletedException("User", "id", String.valueOf(id));
        }

        return user;
    }

    public User getAnyUserById(int id) {
        User user = userRepository.getUserById(id);

        if (user == null) {
            throw new EntityNotFoundException("User", "id", String.valueOf(id));
        }

        return user;
    }

    public User getUserByUsername(String username) {
        User user = userRepository.findUserByUsername(username);

        if (user.isEnabled() == false) {
            throw new EntityDeletedException("User", "username", String.valueOf(username));
        }

        return user;
    }

    public boolean checkUserExists(int id) {
        return userRepository.existsById(id);
    }

    public boolean checkUserExists(String username) {
        return userRepository.existsByUsername(username);
    }

    public boolean checkUserExistsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public boolean checkUserExistsByPhoneNumber(String phoneNumber) {
        return userRepository.existsByPhoneNumber(phoneNumber);
    }

    public User getUserByEmail(String email) {
        User user = userRepository.findUserByEmail(email);

        if (user == null) {
            throw new EntityNotFoundException("User", "email", email);
        }

        if (user.isEnabled() == false) {
            throw new EntityDeletedException("User", "email", String.valueOf(email));
        }

        return userRepository.findUserByEmail(email);
    }

    public User getUserByPhoneNumber(String phoneNumber) {
        User user = userRepository.findUserByPhoneNumber(phoneNumber);

        if (user == null) {
            throw new EntityNotFoundException("User", "phone number", phoneNumber);
        }

        if (user.isEnabled() == false) {
            throw new EntityDeletedException("User", "phone number", String.valueOf(phoneNumber));
        }


        return userRepository.findUserByPhoneNumber(phoneNumber);
    }

    public User updateUser(int id, User user) {
        User nonUpdated = userRepository.getUserById(id);

        if (nonUpdated == null) {
            throw new EntityNotFoundException("User", "id", String.valueOf(id));
        }

        if (user.isEnabled() == false) {
            throw new EntityDeletedException("User", "id", String.valueOf(id));
        }

        userRepository.save(user);

        return user;
    }

    public void deleteUserById(int id) {
        User user = userRepository.getUserById(id);

        if (user == null) {
            throw new EntityNotFoundException("User", "id", String.valueOf(id));
        }

        user.setEnabled(false);

        userRepository.save(user);
    }

    public void activateUser(int id) {
        User user = userRepository.getUserById(id);

        if (user == null) {
            throw new EntityNotFoundException("User", "id", String.valueOf(id));
        }

        user.setBlocked(false);
        user.setEnabled(true);
        userRepository.save(user);
    }

    public void blockUser(int id) {
        User user = userRepository.getUserById(id);

        if (user == null) {
            throw new EntityNotFoundException("User", "id", String.valueOf(id));
        }

        user.setBlocked(true);
        userRepository.save(user);
    }

    public void verifyUser(String username) {
        User user = userRepository.findUserByUsername(username);
        user.setVerified(true);
        userRepository.save(user);
    }

    public List<Wallet> getUserWalletsList(int id) {
        User user = userRepository.getUserById(id);

        if (user == null) {
            throw new EntityNotFoundException("User", "id", String.valueOf(id));
        }

        List<Wallet> wallets = userRepository.findById(id);

        if (wallets.size() == 0) {
            throw new EntityNotFoundException(String.format("User with id: %d doesn't have wallet.", id));
        }

        return wallets;
    }

    public void addCardToUser(String username, Card card) {
        User userToAddCard = userRepository.findUserByUsername(username);
        Card cardToBeAdded = cardRepository.getByCardNumberAndCsv(card.getCardNumber(),card.getCsv());

        if (userToAddCard == null) {
            throw new EntityNotFoundException("User", "id", username);
        }

        if (userToAddCard.isEnabled() == false) {
            throw new EntityDeletedException("User", "id", username);
        }

        userToAddCard.getCards().add(cardToBeAdded);

        userRepository.save(userToAddCard);
    }

    public void addWalletToUser(String username, Wallet wallet) {
        User userToAddWallet = userRepository.findUserByUsername(username);
        Wallet walletToBeAdded = walletRepository.getWalletById(wallet.getId());

        if (userToAddWallet == null) {
            throw new EntityNotFoundException("User", "id", username);
        }

        if (userToAddWallet.isEnabled() == false) {
            throw new EntityDeletedException("User", "id", username);
        }


        userToAddWallet.getWallets().add(walletToBeAdded);

        userRepository.save(userToAddWallet);
    }

    public Wallet getDefaultUserWallet(User user) {
        return user.getWallets()
                .stream()
                .filter(Wallet::isDefault)
                .collect(Collectors.toList())
                .get(0);
    }

    public List<User> getAllUsers(String[] filterArray) {
        List<User> users = userRepository.findAll();

        if(filterArray[0] != null || filterArray[1] != null || filterArray[2] != null) {

            if(filterArray[0] == null) filterArray[0] = "";
            if(filterArray[1] == null) filterArray[1] = "";
            if(filterArray[2] == null) filterArray[2] = "";

            return userRepository.findAllByUsernameLikeAndEmailLikeAndPhoneNumberLike(
                    "%" + filterArray[0] + "%",
                    "%" + filterArray[1] + "%",
                    "%" + filterArray[2] + "%"
            );
        }

        if(filterArray[3] != null) {
            return userRepository.findAllByVerified(Boolean.parseBoolean(filterArray[3]));
        }

        if(filterArray[4] != null) {
            return userRepository.findAllByBlocked(Boolean.parseBoolean(filterArray[4]));
        }

        if(filterArray[5] != null) {
            return userRepository.findAllByEnabled(Boolean.parseBoolean(filterArray[5]));
        }

        return users;
    }

}
