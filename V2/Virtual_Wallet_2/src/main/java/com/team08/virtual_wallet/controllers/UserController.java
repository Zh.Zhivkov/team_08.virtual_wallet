package com.team08.virtual_wallet.controllers;

import com.google.gson.Gson;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.dto.DtoMapper;
import com.team08.virtual_wallet.models.dto.TransactionFilterDto;
import com.team08.virtual_wallet.models.dto.UserDto;
import com.team08.virtual_wallet.services.UserService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController {

    private UserService userService;
    private DtoMapper dtoMapper;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserService userService, DtoMapper dtoMapper, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.dtoMapper = dtoMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/my-profile")
    public String showProfilePage(Model model, Principal principal) {
        if (principal == null) {
            return "login";
        }

        User user = userService.getUserByUsername(principal.getName());
        UserDto userDto = dtoMapper.toUserDto(user);
        UserDto toBeUpdatedUser = dtoMapper.toUserDto(user);
        UserDto passwordResetDto = new UserDto();

        String verified;
        String oldPassword = "null";

        if(user.isVerified()) {
            verified = "Verified";
        } else {
            verified = "Not verified";
        }

        model.addAttribute("user", userDto);
        model.addAttribute("verified", verified);
        model.addAttribute("toBeUpdatedUser", toBeUpdatedUser);
        model.addAttribute("passwordResetDto", passwordResetDto);
        model.addAttribute("oldPassword", oldPassword);

        return "profile-page";
    }

    @PostMapping("/reset-password")
    public ModelAndView resetUserPassword(@ModelAttribute UserDto passwordResetDto, BindingResult bindingResult, Model model, Principal principal) {
        if(principal == null) {
            return new ModelAndView("/login");
        }

        if(!userService.checkUserExistsByEmail(passwordResetDto.getEmail()) || !userService.getUserByEmail(passwordResetDto.getEmail()).getUsername().equals(principal.getName())) {
            model.addAttribute("error", "No user with such email exists or the email does not belong to this account!");
            return new ModelAndView("redirect:/my-profile?wrong_email");
        }

        if(!passwordResetDto.getPassword().equals(passwordResetDto.getPasswordConfirmation())) {
            model.addAttribute("error", "Passwords don't match!");
            return new ModelAndView("redirect:/my-profile?passwords_dont_match");
        }

        User user = userService.getUserByUsername(principal.getName());

        if(!passwordEncoder.matches(passwordResetDto.getFirstName(), user.getPassword())) {
            model.addAttribute("error", "Incorrect password!");
            return new ModelAndView("redirect:/my-profile?incorrect_password");
        }

        user.setPassword(passwordEncoder.encode(passwordResetDto.getPassword()));

        userService.updateUser(user.getId(), user);

        return new ModelAndView("redirect:/my-profile?successful_password_update");
    }

    @PostMapping("/update-profile")
    public ModelAndView updateUserProfile(@Valid @ModelAttribute UserDto updatedUser, BindingResult bindingResult, Model model, Principal principal) {
        if(principal == null) {
            return new ModelAndView("/login");
        }

        if(userService.checkUserExistsByPhoneNumber(updatedUser.getPhoneNumber())) {
            if(userService.getUserByUsername(principal.getName()) != userService.getUserByPhoneNumber(updatedUser.getPhoneNumber())) {
                model.addAttribute("error", "Phone number taken!");
                return new ModelAndView("redirect:/my-profile?phone_number_taken");
            }
        }

        User oldUser = userService.getUserByUsername(principal.getName());

        oldUser.setFirstName(updatedUser.getFirstName());
        oldUser.setLastName(updatedUser.getLastName());
        oldUser.setPhoneNumber(updatedUser.getPhoneNumber());

        if (!updatedUser.getPhotoUpdate().isEmpty()) {
            try {
                oldUser.setPhoto(updatedUser.getPhotoUpdate().getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        userService.updateUser(oldUser.getId(), oldUser);

        return new ModelAndView("redirect:/my-profile?successful_update");
    }

    @GetMapping("/users")
    public ModelAndView showUsers(Principal principal,
                                  @RequestParam(required = false) String username,
                                  @RequestParam (required = false) String phone_number,
                                  @RequestParam (required = false) String email,
                                  @RequestParam (required = false) String verified,
                                  @RequestParam (required = false) String blocked,
                                  @RequestParam (required = false) String enabled) {


        if(principal == null) {
            return new ModelAndView("redirect:/login");
        }

        User user = userService.getUserByUsername(principal.getName());
        if (user.getAuthorities().stream().noneMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"))) {
            return new ModelAndView("redirect:/login");
        }

        String[] filterArray = {username, email, phone_number, verified, blocked, enabled};

        List<User> users = userService.getAllUsers(filterArray).stream().filter(User::isEnabled).collect(Collectors.toList());
        List<User> blockedUsers = users.stream().filter(User::isBlocked).collect(Collectors.toList());
        List<User> disabledUsers = userService.getAllUsers(filterArray).stream().filter(user1 -> !user1.isEnabled()).collect(Collectors.toList());

        ModelAndView userPage = new ModelAndView("user-management");
        userPage.addObject("userFilter", new UserDto());
        userPage.addObject("users", new Gson().toJson(dtoMapper.toUserInfoDto(users)));
        userPage.addObject("blocked_users", new Gson().toJson(dtoMapper.toUserInfoDto(blockedUsers)));
        userPage.addObject("disabled_users", new Gson().toJson(dtoMapper.toUserInfoDto(disabledUsers)));
        return userPage;

    }

    @PostMapping("filter-users")
    public ModelAndView showFilteredUsers(@ModelAttribute UserDto filteredDto) {
        if(filteredDto.getUsername() == null) filteredDto.setUsername("");
        if(filteredDto.getPhoneNumber() == null) filteredDto.setPhoneNumber("");
        if(filteredDto.getEmail() == null) filteredDto.setEmail("");

        return new ModelAndView(
                String.format("redirect:/users?username=%s&phone_number=%s&email=%s",
                filteredDto.getUsername(),
                filteredDto.getPhoneNumber(),
                filteredDto.getEmail()));
    }

    @PostMapping("/users/activate/{id}")
    public ModelAndView activateUser(@PathVariable String id, Principal principal) {
        if(principal == null) {
            return new ModelAndView("redirect:/login");
        }

        User admin = userService.getUserByUsername(principal.getName());

        if (admin.getAuthorities().stream().noneMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"))) {
            return new ModelAndView("redirect:/login");
        }

        int idInt = Integer.parseInt(id);

        if(!userService.checkUserExists(idInt)) {
            return new ModelAndView("redirect:/users?invalid_id");
        }

        User toBeActivated = userService.getAnyUserById(idInt);

        if(!toBeActivated.isBlocked() && toBeActivated.isEnabled()) {
            return new ModelAndView("redirect:/users?user_already_active");
        }

        userService.activateUser(idInt);

        return new ModelAndView("redirect:/users?successful_activation");
    }

    @PostMapping("/users/block/{id}")
    public ModelAndView blockUser(@PathVariable String id, Principal principal) {
        if(principal == null) {
            return new ModelAndView("redirect:/login");
        }

        User admin = userService.getUserByUsername(principal.getName());

        if (admin.getAuthorities().stream().noneMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"))) {
            return new ModelAndView("redirect:/login");
        }

        int idInt = Integer.parseInt(id);

        if(!userService.checkUserExists(idInt)) {
            return new ModelAndView("redirect:/users?invalid_id");
        }

        User toBeBlocked = userService.getUserById(idInt);

        if(toBeBlocked.isBlocked()) {
            return new ModelAndView("redirect:/users?user_already_blocked");
        }

        userService.blockUser(idInt);

        return new ModelAndView("redirect:/users?successful_block");
    }

    @PostMapping("/users/disable/{id}")
    public ModelAndView disableUser(@PathVariable String id, Principal principal) {
        if(principal == null) {
            return new ModelAndView("redirect:/login");
        }

        User admin = userService.getUserByUsername(principal.getName());

        if (admin.getAuthorities().stream().noneMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"))) {
            return new ModelAndView("redirect:/login");
        }

        int idInt = Integer.parseInt(id);

        if(!userService.checkUserExists(idInt)) {
            return new ModelAndView("redirect:/users?invalid_id");
        }

        User toBeDisabled = userService.getAnyUserById(idInt);

        if(!toBeDisabled.isEnabled()) {
            return new ModelAndView("redirect:/users?user_already_disabled");
        }

        userService.deleteUserById(idInt);

        return new ModelAndView("redirect:/users?successful_disable");
    }
}
