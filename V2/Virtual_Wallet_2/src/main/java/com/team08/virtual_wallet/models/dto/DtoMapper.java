package com.team08.virtual_wallet.models.dto;

import com.team08.virtual_wallet.exceptions.EntityNotFoundException;
import com.team08.virtual_wallet.models.*;
import com.team08.virtual_wallet.services.UserService;
import com.team08.virtual_wallet.services.WalletServiceImpl;
import com.team08.virtual_wallet.services.contracts.CardService;
import com.team08.virtual_wallet.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Component
public class DtoMapper {

    private UserService userService;
    private WalletServiceImpl walletService;
    private CardService cardService;


    @Autowired
    public DtoMapper(UserService userService, WalletServiceImpl walletService, CardService cardService) {
        this.userService = userService;
        this.walletService = walletService;
        this.cardService = cardService;
    }

    public User fromUserDto(UserDto userDto) {

        User user = new User();

        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setPassword(userDto.getPassword());
        user.setFirstName("Default");
        user.setLastName("Default");
        user.setEnabled(true);
        user.setBlocked(false);
        user.setVerified(false);

        try {
            user.setPhoto(Base64.getDecoder().decode(Constants.DEFAULT_USER_PROFILE_PICTURE.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return user;
    }

    public UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setPhoto(Base64.getEncoder().encodeToString(user.getPhoto()));

        return userDto;
    }

    public List<UserDto> toUserDto(List<User> users) {
        List<UserDto> transformed = new ArrayList<>();
        for (User user : users) {
            transformed.add(toUserDto(user));
        }
        return transformed;
    }

    public CardNumberDto toCardNumberDto(Card card) {
        CardNumberDto cardNumberDto = new CardNumberDto();

        String cardNumber = hideCardNumber(card);

        cardNumberDto.setCardNumber(cardNumber);

        return cardNumberDto;
    }



    public Card fromCardDetailsDto(CardDetails cardDetails) {
        Card card = new Card();

        card.setCardholderName(cardDetails.getCardholderName());
        card.setCardNumber(cardDetails.getCardNumber());
        card.setExpirationDate(cardDetails.getExpirationDate());
        card.setCsv(cardDetails.getCsv());

        return card;
    }

    public CardDetails toCardDetailsDto(Card card) {
        CardDetails cardDetails = new CardDetails();

        cardDetails.setCardholderName(card.getCardholderName());
        cardDetails.setCardNumber(card.getCardNumber());
        cardDetails.setExpirationDate(card.getExpirationDate());
        cardDetails.setCsv(card.getCsv());

        return cardDetails;
    }

    public Card fromCardDto(CardDto cardDto) {
        Card card = new Card();

        card.setId(cardDto.getId());
        card.setCardholderName(cardDto.getCardholderName());
        card.setCardNumber(cardDto.getCardNumber());
        card.setExpirationDate(cardDto.getExpirationDate());

        return card;
    }

    public CardDto toCardDto(Card card) {
        CardDto cardDto = new CardDto();

        cardDto.setId(card.getId());
        cardDto.setCardholderName(card.getCardholderName());

        String cardNumber = hideCardNumber(card);

        cardDto.setCardNumber(cardNumber);
        cardDto.setExpirationDate(card.getExpirationDate());

        return cardDto;
    }

    public Card fromCardEditDto(CardEditDto cardEditDto) {
        Card card = new Card();

        card.setId(cardEditDto.getId());
        card.setCsv(cardEditDto.getCsv());
        card.setCardholderName(cardEditDto.getCardholderName());
        card.setCardNumber(cardEditDto.getCardNumber());
        card.setExpirationDate(cardEditDto.getExpirationDate());
        card.setUser(userService.getUserByUsername(cardEditDto.getUserName()));

        return card;
    }

    public CardEditDto toCardEditDto(Card card) {
        CardEditDto cardEditDto = new CardEditDto();

        cardEditDto.setId(card.getId());
        cardEditDto.setCsv(card.getCsv());
        cardEditDto.setCardholderName(card.getCardholderName());
        cardEditDto.setCardNumber(card.getCardNumber());
        cardEditDto.setExpirationDate(card.getExpirationDate());
        cardEditDto.setUserName(card.getUser().getUsername());

        return cardEditDto;
    }

    public WalletEditDto toWalletEditDto(Wallet wallet) {
        WalletEditDto walletEditDto = new WalletEditDto();

        walletEditDto.setOldNAme(walletEditDto.getName());
        walletEditDto.setId(wallet.getId());
        walletEditDto.setBalance(wallet.getBalance());
        walletEditDto.setUserName(wallet.getUser().getUsername());
        walletEditDto.setName(wallet.getName());
        walletEditDto.setDefault(wallet.isDefault());
        walletEditDto.setEnabled(wallet.isEnabled());

        return walletEditDto;
    }

    public Wallet fromWalletEditDto(WalletEditDto walletEditDto) {
        Wallet wallet = new Wallet();

        wallet.setName(walletEditDto.getName());
        wallet.setId(walletEditDto.getId());
        wallet.setDefault(walletEditDto.isDefault());
        wallet.setBalance(walletEditDto.getBalance());
        wallet.setUser(userService.getUserByUsername(walletEditDto.getUserName()));
        wallet.setEnabled(walletEditDto.isEnabled());

        return wallet;
    }

    public Transaction fromTransactionDto(TransactionDto transactionDto) {
        Transaction transaction = new Transaction();
        transaction.setAmount(transactionDto.getAmount());
        transaction.setSenderWallet(walletService.getWalletById(transactionDto.getSenderWalletId()));
        transaction.setRecipient(userService.getUserById(transactionDto.getRecipientId()));
        return transaction;
    }

    public BankTransfer fromBankTransferDto(BankTransferDto bankTransferDto, User user) {
        BankTransfer bankTransfer = new BankTransfer();
        bankTransfer.setAmount(bankTransferDto.getAmount() * 100);
        bankTransfer.setCurrency("BGN");
        bankTransfer.setDescription(Constants.TRANSFER_DESCRIPTION);

        String lastDigits = bankTransferDto.getCardNumberLastDigits();

        Card card = cardService.
                findUserCardByCardNumberContaining(user, lastDigits
                        .substring(lastDigits.length() - 4));
        if (card == null) {
            throw new EntityNotFoundException("Card", "last 4 digits", bankTransferDto.getCardNumberLastDigits());
        }

        CardDetails cardDetails = toCardDetailsDto(card);
        bankTransfer.setCardDetails(cardDetails);
        bankTransfer.setIdempotencyKey(generateIdempotencyKey(bankTransfer));


        return bankTransfer;
    }

    public UserDto toUserInfoDto(User user) {
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setBlocked(user.isBlocked());
        userDto.setEnabled(user.isEnabled());

        return userDto;
    }

    public List<UserDto> toUserInfoDto(List<User> users) {
        List<UserDto> transformed = new ArrayList<>();
        for (User user: users) {
            transformed.add(toUserInfoDto(user));
        }
        return transformed;
    }

    private String generateIdempotencyKey(BankTransfer bankTransfer) {
        StringBuilder builder = new StringBuilder("");
        builder.append(bankTransfer.getCardDetails().getCardholderName())
                .append(bankTransfer.getCardDetails().getExpirationDate())
                .append(LocalDateTime.now());

        return builder.toString();
    }

    private String hideCardNumber(Card card) {
        String cardNumber = card.getCardNumber();
        if (cardNumber.length() > 4) {
            cardNumber = "****-****-****-" + cardNumber.substring(cardNumber.length() - 4);
        }
        return cardNumber;
    }



    public TransactionDisplayDto toTransactionDisplayDto(Transaction transaction) {
        TransactionDisplayDto dto = new TransactionDisplayDto();
        dto.setId(String.valueOf(transaction.getId()));
        dto.setAmount(String.valueOf(transaction.getAmount()));
        dto.setSender(transaction.getSender().getUsername());
        dto.setRecipient(transaction.getRecipient().getUsername());
        dto.setDate(transaction.getDate().toString());

        if (transaction.getStatus() == 0) {
            dto.setStatus("unsuccessful");
        } else {
            dto.setStatus("successful");
        }

        return dto;
    }

    public List<TransactionDisplayDto> transactionDisplayDtos(List<Transaction> transactions) {
        List<TransactionDisplayDto> dtos = new ArrayList<>();
        for(Transaction transaction : transactions) {
            dtos.add(toTransactionDisplayDto(transaction));
        }
        return dtos;
    }

    public List<TransactionDisplayDto> transactionDisplayDtos(User user, List<Transaction> transactions) {
        List<TransactionDisplayDto> dtos = new ArrayList<>();
        for(Transaction transaction : transactions) {
            TransactionDisplayDto dto = toTransactionDisplayDto(transaction);
            if(transaction.getRecipient().getId() == user.getId()) {
                dto.setDirection("incoming");
            } else {
                dto.setDirection("outgoing");
            }
            dtos.add(dto);
        }
        return dtos;
    }

}
