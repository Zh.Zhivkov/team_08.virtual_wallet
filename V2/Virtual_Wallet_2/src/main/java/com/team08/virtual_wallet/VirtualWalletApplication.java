package com.team08.virtual_wallet;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@SpringBootApplication
public class VirtualWalletApplication {

    private static final Logger log = LoggerFactory.getLogger(VirtualWalletApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(VirtualWalletApplication.class, args);
    }

    @Bean
    public Docket swaggerConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.team_08.virtual_wallet"))
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "Virtual Wallet API",
                "Virtual Wallet API, final project",
                "1.0",
                "Free to use",
                new springfox.documentation.service.Contact
                        ("Zhivko Zhivkov", "http://virtual_wallet.team_08.com", "zhivko@gmail.com"),
                "API License",
                "http://virtual_wallet.team_08.com",
                Collections.emptyList());
    }
}
