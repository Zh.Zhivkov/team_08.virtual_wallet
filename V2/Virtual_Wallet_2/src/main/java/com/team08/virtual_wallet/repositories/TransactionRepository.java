package com.team08.virtual_wallet.repositories;


import com.team08.virtual_wallet.models.Transaction;
import com.team08.virtual_wallet.models.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    List<Transaction> findAll();

    List<Transaction> findAllByOrderByAmountAsc();

    List<Transaction> findAllByOrderByAmountDesc();

    List<Transaction> findAllByOrderByDateAsc();

    List<Transaction> findAllByOrderByDateDesc();

    List<Transaction> findAllByDateBetweenOrderByAmountAsc(LocalDateTime start, LocalDateTime end);
    List<Transaction> findAllByDateBetweenOrderByAmountDesc(LocalDateTime start, LocalDateTime end);
    List<Transaction> findAllByDateBetweenOrderByDateAsc(LocalDateTime start, LocalDateTime end);
    List<Transaction> findAllByDateBetweenOrderByDateDesc(LocalDateTime start, LocalDateTime end);
}
