package com.team08.virtual_wallet.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class CardDto {

    private int id;

    private String cardNumber;

    private String expirationDate;

    private String cardholderName;

}
