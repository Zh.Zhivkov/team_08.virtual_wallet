package com.team08.virtual_wallet.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.Size;

@Data @NoArgsConstructor
public class CardDetails {

    private String cardNumber;

    private String expirationDate;

    private String cardholderName;

    private String csv;
}
