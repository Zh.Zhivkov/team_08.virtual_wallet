package com.team08.virtual_wallet.repositories;

import com.team08.virtual_wallet.models.ConfirmationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, Integer> {
    ConfirmationToken findByTokenString(String confirmationToken);

    boolean existsConfirmationTokenByTokenString(String tokenString);
}
