package com.team08.virtual_wallet.controllers;

import com.team08.virtual_wallet.models.ConfirmationToken;
import com.team08.virtual_wallet.models.GrantedAuthority;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.dto.DtoMapper;
import com.team08.virtual_wallet.models.dto.UserDto;
import com.team08.virtual_wallet.repositories.ConfirmationTokenRepository;
import com.team08.virtual_wallet.services.AuthorityService;
import com.team08.virtual_wallet.services.EmailSenderService;
import com.team08.virtual_wallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.util.Properties;

@RequestMapping("/register/**")
@Controller
public class RegistrationController {

    private UserService userService;
    private AuthorityService authorityService;
    private DtoMapper dtoMapper;
    private PasswordEncoder passwordEncoder;
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    public RegistrationController(UserService userService, DtoMapper dtoMapper, PasswordEncoder passwordEncoder, AuthorityService authorityService, ConfirmationTokenRepository confirmationTokenRepository) {
        this.userService = userService;
        this.dtoMapper = dtoMapper;
        this.passwordEncoder = passwordEncoder;
        this.authorityService = authorityService;
        this.confirmationTokenRepository = confirmationTokenRepository;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public ModelAndView registerUser(@Valid @ModelAttribute UserDto userDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("redirect:/register?binding_errors");
        }

        if(userService.checkUserExists(userDto.getUsername())) {
            model.addAttribute("error", "User with the same username already exists!");
            return new ModelAndView("redirect:/register?username_taken");
        }

        if(userService.checkUserExistsByEmail(userDto.getEmail())) {
            model.addAttribute("error", "User with the same email already exists!");
            return new ModelAndView("redirect:/register?email_taken");
        }

        if(userService.checkUserExistsByPhoneNumber(userDto.getPhoneNumber())) {
            model.addAttribute("error", "User with the same phone number already exists!");
            return new ModelAndView("redirect:/register?phone_number_taken");
        }

        User user = dtoMapper.fromUserDto(userDto);
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));

        userService.creteUser(user);
        GrantedAuthority userAuthority = new GrantedAuthority(user.getId(), user.getUsername(), "ROLE_USER");
        user.getAuthorities().add(userAuthority);
        authorityService.createAuthority(userAuthority);

        ConfirmationToken confirmationToken = new ConfirmationToken(user);

        confirmationTokenRepository.save(confirmationToken);

//        SimpleMailMessage mailMessage = new SimpleMailMessage();
//        mailMessage.setTo(user.getEmail());
//        mailMessage.setSubject("Complete Registration!");
//        mailMessage.setFrom("chand312902@gmail.com");
//        mailMessage.setText("To confirm your account, please click here : "
//                +"http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationToken());
//
//        emailSenderService.sendEmail(mailMessage);

        final String username = "emi.metushev98@gmail.com";
        final String password = "zgqmztnmjioyymxv";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        Session session1 = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session1);
            message.setFrom(new InternetAddress("wallet_mail@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail()));

            message.setSubject("Complete Registration!");
            message.setText(String.format("Dear %s,%n%n You have successfully registered! Before accessing your virtual wallet, you need to verify your account.%n In order to do so, please click here : ", user.getUsername())
                + "http://localhost:8080/confirm-account?token=" + confirmationToken.getTokenString() + String.format("%n%nWe are glad to have you,%n%nYours sincerely,%n%nThe Virtual Wallet Team"));

            Transport.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ModelAndView modelAndView = new ModelAndView("/register-confirmation");
        modelAndView.addObject("email", user.getEmail());
        modelAndView.addObject("username", user.getUsername());

        return modelAndView;
    }

//    @RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})


    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }
}
