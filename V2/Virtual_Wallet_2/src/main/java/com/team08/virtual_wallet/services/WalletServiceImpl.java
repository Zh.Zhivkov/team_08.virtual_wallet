package com.team08.virtual_wallet.services;

import com.google.gson.Gson;
import com.team08.virtual_wallet.exceptions.EntityDeletedException;
import com.team08.virtual_wallet.exceptions.EntityNotFoundException;
import com.team08.virtual_wallet.models.BankTransfer;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import com.team08.virtual_wallet.models.dto.DtoMapper;
import com.team08.virtual_wallet.models.dto.WalletEditDto;
import com.team08.virtual_wallet.repositories.*;
import com.team08.virtual_wallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WalletServiceImpl implements WalletService {


    private final WalletRepository walletRepository;
    private final UserRepository userRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository, UserRepository userRepository) {
        this.walletRepository = walletRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Wallet createWallet(Wallet wallet, String username) {

        User user = userRepository.findUserByUsername(username);

        wallet.setBalance(0);
        wallet.setUser(user);
        wallet.setEnabled(true);

        if (user.getWallets().isEmpty()) {
            wallet.setDefault(true);
        }

        if (wallet.isDefault()) {
            user.getWallets().
                    forEach(w -> {w.setDefault(false); walletRepository.save(w);});

            wallet.setDefault(true);
        }


        return walletRepository.save(wallet);
    }

    @Override
    public Wallet getWalletById(int id) {
        Wallet wallet = walletRepository.getWalletById(id);

        if (wallet == null) {
            throw new EntityNotFoundException("Wallet", "id", String.valueOf(id));
        }

        if (wallet.isEnabled() == false) {
            throw new EntityDeletedException("Wallet", "id", String.valueOf(id));
        }

        return wallet;
    }

    @Override
    public Wallet getWalletByName(String name) {
        Wallet wallet = walletRepository.getWalletByName(name);

        if (wallet == null) {
            throw new EntityNotFoundException("Wallet", "name", name);
        }

        if (wallet.isEnabled() == false) {
            throw new EntityDeletedException("Wallet", "name", name);
        }

        return wallet;
    }

    @Override
    public Wallet editWallet(int id, WalletEditDto walletEditDto) {
        Wallet walletToBeUpdated = walletRepository.getWalletById(id);

        if (walletToBeUpdated.isEnabled() == false) {
            throw new EntityDeletedException("Wallet", "name", String.valueOf(id));
        }

        User user = userRepository.findUserByUsername(walletEditDto.getUserName());

        walletToBeUpdated.setName(walletEditDto.getName());
        walletToBeUpdated.setDefault(walletEditDto.isDefault());
        walletToBeUpdated.setUser(user);

        walletRepository.save(walletToBeUpdated);

        return walletToBeUpdated;
    }

    @Override
    public Wallet updateWallet(int id, Wallet wallet) {
        Wallet walletToBeUpdated = walletRepository.getWalletById(id);

        if (walletToBeUpdated == null) {
            throw new EntityNotFoundException("Wallet", "id", String.valueOf(id));
        }

        if (walletToBeUpdated.isEnabled() == false) {
            throw new EntityDeletedException("Wallet", "id", String.valueOf(id));
        }

        walletRepository.save(wallet);

        return wallet;
    }

    @Override
    public void deleteWallet(int id, String username) {
        Wallet wallet = walletRepository.getWalletById(id);

        if (wallet == null) {
            throw new EntityNotFoundException("Wallet", "id", String.valueOf(id));
        }


        wallet.setEnabled(false);

        User user = userRepository.findUserByUsername(username);

        List<Wallet> defaultWallet = user.getWallets().stream()
                .filter(Wallet::isDefault)
                .collect(Collectors.toList());

        defaultWallet.get(0).setBalance(defaultWallet.get(0).getBalance() + wallet.getBalance());

        walletRepository.save(wallet);

    }

    @Override
    public void transferMoneyFromCardToWallet(int amount, int walletId) {
        Wallet wallet = walletRepository.getWalletById(walletId);

        if (wallet == null) {
            throw new EntityNotFoundException("Wallet", "id", String.valueOf(walletId));
        }

        double sumToAdd = amount / 100;

        wallet.setBalance(wallet.getBalance() + sumToAdd);
        walletRepository.save(wallet);
    }

    @Override
    public HttpStatus requestToApi(BankTransfer bankTransfer) {

        RestTemplate restTemplate = new RestTemplate();
//        JSONObject jsonObject = new JSONObject(bankTransfer);
        String jsonObject = new Gson().toJson(bankTransfer);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("x-api-key", bankTransfer.getIdempotencyKey());

        HttpEntity<String> request = new HttpEntity<>(jsonObject, headers);

        return restTemplate.postForEntity("http://localhost:8081/payment", request, String.class).getStatusCode();
    }

    @Override
    public List<Wallet> getAllWallets(User user, String[] filterArray) {

        if (filterArray[0] != null && filterArray[1] != null) {
            if (filterArray[1].equals("balance_asc")) {
                return walletRepository.findAllByUserAndNameLikeAndEnabledTrueOrderByBalanceAsc(
                        user, "%" + filterArray[0] + "%");
            }
            if (filterArray[1].equals("balance_desc")) {
                return walletRepository.findAllByUserAndNameLikeAndEnabledTrueOrderByBalanceDesc(
                        user, "%" + filterArray[0] + "%"
                );
            }
        }

        if (filterArray[0] != null) {
            return walletRepository.findAllByUserAndNameLike(
                    user, "%" + filterArray[0] + "%");
        }

        if (filterArray[1] != null) {
            if (filterArray[1].equals("balance_asc")) {
                return walletRepository.findAllByUserAndEnabledTrueOrderByBalanceAsc(user);
            }
            if (filterArray[1].equals("balance_desc")) {
                return walletRepository.findAllByUserAndEnabledTrueOrderByBalanceDesc(user);
            }
        }

        return walletRepository.findAllByUserAndEnabledTrue(user);
    }

    @Override
    public boolean checkIfWalletNameExist(String walletName, String username) {
        User user = userRepository.findUserByUsername(username);

        return user.getWallets().stream()
                .anyMatch(wallet -> wallet.getName().equals(walletName));
    }

    @Override
    public boolean checkWalletBelongingByName(String walletName, String username) {
        User user = userRepository.findUserByUsername(username);

        return user.getWallets().stream()
                .anyMatch(wallet -> wallet.getName().equals(walletName));
    }

    @Override
    public void setDefaultWallet(Wallet oldDefault, int newDefaultId) {
        Wallet newDefault = getWalletById(newDefaultId);
        oldDefault.setDefault(false);
        newDefault.setDefault(true);
        walletRepository.save(oldDefault);
        walletRepository.save(newDefault);
    }

}
