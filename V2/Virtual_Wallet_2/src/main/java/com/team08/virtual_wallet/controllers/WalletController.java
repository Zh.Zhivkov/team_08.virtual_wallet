package com.team08.virtual_wallet.controllers;

import com.team08.virtual_wallet.exceptions.InsufficientFundsException;
import com.team08.virtual_wallet.exceptions.UnauthorizedUserException;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import com.team08.virtual_wallet.models.dto.WalletFilterDto;
import com.team08.virtual_wallet.exceptions.EntityNotFoundException;
import com.team08.virtual_wallet.models.BankTransfer;
import com.team08.virtual_wallet.models.Card;
import com.team08.virtual_wallet.models.dto.*;
import com.team08.virtual_wallet.services.UserService;
import com.team08.virtual_wallet.services.contracts.CardService;
import com.team08.virtual_wallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class WalletController {

    private final WalletService walletService;
    private final UserService userService;
    private final CardService cardService;
    private final DtoMapper dtoMapper;

    @Autowired
    public WalletController(WalletService walletService, UserService userService, CardService cardService, DtoMapper dtoMapper) {
        this.walletService = walletService;
        this.userService = userService;
        this.cardService = cardService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/make-BankTransfer")
    public ModelAndView showTransactionPage(Principal principal) {

        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        if (!userService.getUserByUsername(principal.getName()).isVerified()) {
            return new ModelAndView("redirect:/my-profile?error_not_verified");
        }

        if (userService.getUserByUsername(principal.getName()).isBlocked()) {
            return new ModelAndView("redirect:/my-profile?error_blocked_transaction");
        }

        ModelAndView page = new ModelAndView("make-BankTransfer");

        List<Wallet> wallets = userService.getUserByUsername(principal.getName()).getWallets().stream().filter(Wallet::isEnabled).collect(Collectors.toList());

        page.addObject("wallets", wallets);

        List<CardNumberDto> list = new ArrayList<>();
        for (Card card : userService.getUserByUsername(principal.getName()).getCards()) {
            list.add(dtoMapper.toCardNumberDto(card));
        }

        page.addObject("cards", list);
        page.addObject("bankTransfer", new BankTransferDto());
        return page;
    }

    @PostMapping("/make-BankTransfer")
    public ModelAndView transferMoneyToWallet(@ModelAttribute BankTransferDto bankTransferDto,
                                              BindingResult bindingResult,
                                              Principal principal) {

        if (bindingResult.hasErrors()) {
            return new ModelAndView("redirect:/make-BankTransfer?binding_errors");
        }

        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        BankTransfer bankTransfer = dtoMapper.fromBankTransferDto(bankTransferDto, userService.getUserByUsername(principal.getName()));

        try {
            HttpStatus responseEntity = walletService.requestToApi(bankTransfer);
            if (responseEntity == HttpStatus.OK) {
                walletService.transferMoneyFromCardToWallet(bankTransfer.getAmount(), bankTransferDto.getWalletId());
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (HttpClientErrorException.Forbidden e) {
            return new ModelAndView("redirect:/make-BankTransfer?insufficient_funds");
        } catch (HttpClientErrorException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return new ModelAndView("redirect:/make-BankTransfer?success");
    }


    @GetMapping("/my-wallets")
    public ModelAndView showUserWallets(Principal principal,
                                        @RequestParam(required = false) String name,
                                        @RequestParam(required = false) String sort) {

        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        User user = userService.getUserByUsername(principal.getName());

        String[] filterArray = {name, sort};

        ModelAndView walletPage = new ModelAndView("user-wallets");
        walletPage.addObject("walletFilterDto", new WalletFilterDto());
        walletPage.addObject("wallets", walletService.getAllWallets(user, filterArray));

        return walletPage;
    }

    @PostMapping("filter-wallets")
    public ModelAndView showFilteredWallets(@ModelAttribute WalletFilterDto dto) {
        StringBuilder filterOptions = new StringBuilder("?");

        boolean filterByName = false;

        if (dto.getName() != null) {
            filterOptions.append("name=").append(dto.getName());
            filterByName = true;
        }

        if (filterByName) {
            filterOptions.append("&");
        }

        filterOptions.append("sort=").append(dto.getSortOption());

        return new ModelAndView("redirect:/my-wallets" + filterOptions.toString());
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/add-wallet")
    public ModelAndView showAddCardPage(Principal principal) {

        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        ModelAndView addCard = new ModelAndView("add-wallet");
        addCard.addObject("wallet", new Wallet());
        return addCard;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/add-wallet")
    public ModelAndView addWallet(@Valid @ModelAttribute Wallet wallet, BindingResult bindingResult, Principal principal) {

        if(bindingResult.hasErrors()) {
            return new ModelAndView("redirect:/add-wallet?binding_errors");
        }

        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        if(userService.getUserByUsername(principal.getName()).getWallets().stream().anyMatch(wallet1 -> wallet1.getName().equals(wallet.getName()))) {
            return new ModelAndView("redirect:/add-wallet?name_already_used");
        }

//        if (walletService.che(card.getId())) {
//            model.addAttribute("error", "Card with the same id already exists!");
//            return new ModelAndView("redirect:/addCard?card exist");
//        }
//
//        if (cardService.checkCardIsUnique(card.getCardNumber(), card.getCsv())) {
//            model.addAttribute("error", "This card is already registered!");
//            return new ModelAndView("redirect:/addCard?card exist");
//        }
        Wallet walletToBeSaved = walletService.createWallet(wallet, principal.getName());
        userService.addWalletToUser(principal.getName(), walletToBeSaved);

        ModelAndView modelAndView = new ModelAndView("redirect:/my-wallets");

        return modelAndView;
    }


    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/update-wallet/{id}")
    public ModelAndView showWalletToUpdateForms(@PathVariable int id, Principal principal) {
        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        Wallet wallet = walletService.getWalletById(id);

//        if (walletService.checkIfWalletNameExist(wallet.getName(), principal.getName())) {
//            return new ModelAndView("redirect:/my-wallets?invalid_wallet_name");
//        }

        if (!walletService.checkWalletBelongingByName(wallet.getName(), principal.getName())) {
            return new ModelAndView("redirect:/my-wallets?this wallet belongs to other");
        }

        ModelAndView update = new ModelAndView("update-wallet");

        update.addObject("walletUpdate", dtoMapper.toWalletEditDto(wallet));

        return update;
    }

    @PostMapping("/update-wallet/{id}")
    public ModelAndView editWallet(@Valid @ModelAttribute("walletUpdate") WalletEditDto updatedWallet,
                                   @PathVariable int id,
                                   Model model, Principal principal) {
        if (principal == null) {
            return new ModelAndView("/login");
        }

        if (walletService.checkIfWalletNameExist(updatedWallet.getName(), principal.getName())) {
            model.addAttribute("error", "You already have wallet with the same name!");
            return new ModelAndView("redirect:/my-cards?wallet name exist");
        }

        updatedWallet.setUserName(principal.getName());

        walletService.editWallet(id, updatedWallet);

        return new ModelAndView("redirect:/my-wallets");
    }


    @PostMapping("my-wallets/make-default/{wallet_id}")
    public ModelAndView makeWalletDefault(Principal principal, @PathVariable String wallet_id) {
        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        int walletId = Integer.parseInt(wallet_id);

        User user = userService.getUserByUsername(principal.getName());

        if (user.getWallets().stream().noneMatch(wallet -> wallet.getId() == walletId)) {
            return new ModelAndView("redirect:/my-wallets?invalid_wallet_id");
        }

        Wallet currentDefaultWallet = userService.getDefaultUserWallet(user);

        if (currentDefaultWallet.getId() == walletId) {
            return new ModelAndView("redirect:/my-wallets?already_default");
        }

        walletService.setDefaultWallet(currentDefaultWallet, walletId);

        return new ModelAndView("redirect:/my-wallets?successful_new_default");
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("my-wallets/delete-wallet/{wallet_id}")
    public ModelAndView deleteWallet(@PathVariable String wallet_id, Principal principal) {
        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        int walletId = Integer.parseInt(wallet_id);

        Wallet wallet = walletService.getWalletById(walletId);

        if (wallet.isDefault()){
            return new ModelAndView("redirect:/my-wallets?default_wallet_delete");
        }

//        if (walletService.checkIfWalletNameExist(wallet.getName(), principal.getName())) {
//            return new ModelAndView("redirect:/my-wallets?invalid_wallet_name");
//        }

//        if (walletService.checkWalletBelongingByName(wallet.getName(), principal.getName())) {
//            return new ModelAndView("redirect:/my-wallets?this wallet belongs to other");
//        }

        walletService.deleteWallet(walletId,principal.getName());

        return new ModelAndView("redirect:/my-wallets?successful_delete");
    }
}
