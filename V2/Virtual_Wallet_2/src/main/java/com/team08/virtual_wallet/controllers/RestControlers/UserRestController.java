package com.team08.virtual_wallet.controllers.RestControlers;

import com.team08.virtual_wallet.exceptions.DuplicateEntityException;
import com.team08.virtual_wallet.exceptions.EntityDeletedException;
import com.team08.virtual_wallet.exceptions.EntityNotFoundException;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import com.team08.virtual_wallet.models.dto.DtoMapper;
import com.team08.virtual_wallet.models.dto.UserDto;
import com.team08.virtual_wallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private DtoMapper dtoMapper;

    @Autowired
    public UserRestController(UserService userService, DtoMapper dtoMapper) {
        this.userService = userService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/pageable")
    public List<User> getAllUsers(Pageable pageable) {
        return userService.getAllUsers(pageable);
    }

    @CrossOrigin
    @GetMapping
    public List<UserDto> getAllUsers(@RequestParam (required = false) String username, @RequestParam (required = false) String phoneNumber, @RequestParam (required = false) String email, @RequestParam (required = false) String enabled, HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        List<User> toBeReturned = new ArrayList<>();
        if (username != null) {
            toBeReturned = userService.getUsersByUsername(username);
        }
        if (phoneNumber != null) {
            toBeReturned = userService.getUsersByPhoneNumber(phoneNumber);
        }
        if(email != null) {
            toBeReturned = userService.getUsersByEmail(email);
        }
        if(enabled != null) {
            if(enabled.equals("true")) {
                toBeReturned = toBeReturned.stream().filter(User::isEnabled).collect(Collectors.toList());
            }
        }

        return dtoMapper.toUserDto(toBeReturned);
    }

    @PostMapping()
    public User createUser(@RequestBody @Valid User user) {
        try {
            return userService.creteUser(user);
        } catch (DuplicateEntityException dup) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dup.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id) {
        try {
            return userService.getUserById(id);
        } catch (EntityNotFoundException ent) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ent.getMessage());
        } catch (EntityDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{username}")
    public User getUserByUsername(@PathVariable String username) {
        try {
            return userService.getUserByUsername(username);
        } catch (EntityNotFoundException ent) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ent.getMessage());
        } catch (EntityDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{email}")
    public User getUserByEmail(@PathVariable String email) {
        try {
            return userService.getUserByEmail(email);
        } catch (EntityNotFoundException ent) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ent.getMessage());
        } catch (EntityDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{phoneNumber}")
    public User getUserByPhoneNumber(@PathVariable String phoneNumber) {
        try {
            return userService.getUserByPhoneNumber(phoneNumber);
        } catch (EntityNotFoundException ent) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ent.getMessage());
        } catch (EntityDeletedException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable int id,
                           @RequestBody @Valid User user) {
        try {
            return userService.updateUser(id, user);
        } catch (EntityNotFoundException ent) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ent.getMessage());
        } catch (EntityDeletedException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id) {
        try {
            userService.deleteUserById(id);
        } catch (EntityNotFoundException ent) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ent.getMessage());
        }
    }

    @GetMapping("/{id}/walletList")
    public List<Wallet> getWalletsByUser(@PathVariable int id){
        try {
            return userService.getUserWalletsList(id);
        } catch (EntityNotFoundException ent) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ent.getMessage());
        }
    }

}
