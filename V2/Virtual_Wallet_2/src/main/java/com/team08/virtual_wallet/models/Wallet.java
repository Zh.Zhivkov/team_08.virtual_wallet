package com.team08.virtual_wallet.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private double balance;

    private String name;

    @ManyToOne(targetEntity = User.class,fetch = FetchType.EAGER)
    private User user;

    private boolean isDefault;

    private boolean enabled;

    public Wallet(int id) {
        this.id = id;
    }

}

