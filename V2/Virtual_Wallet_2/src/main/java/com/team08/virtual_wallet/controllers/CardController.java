package com.team08.virtual_wallet.controllers;

import com.team08.virtual_wallet.models.*;
import com.team08.virtual_wallet.models.dto.*;
import com.team08.virtual_wallet.services.UserService;
import com.team08.virtual_wallet.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Controller
public class CardController {

    private final CardService cardService;
    private final UserService userService;
    private final DtoMapper dtoMapper;

    @Autowired
    public CardController(CardService cardService, UserService userService, DtoMapper dtoMapper) {
        this.cardService = cardService;
        this.userService = userService;
        this.dtoMapper = dtoMapper;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/my-cards")
    public ModelAndView showUserCards(Principal principal) {

        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        if (!userService.getUserByUsername(principal.getName()).isVerified()) {
            return new ModelAndView("redirect:/my-profile?error_not_verified");
        }

        List<CardDto> list = new ArrayList<>();
        for (Card card : userService.getUserByUsername(principal.getName()).getCards()) {
            list.add(dtoMapper.toCardDto(card));
        }



        ModelAndView cardsPage = new ModelAndView("user-cards");
        cardsPage.addObject("cards", list);

        return cardsPage;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/add-card")
    public ModelAndView showAddCardPage(Principal principal) {

        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        ModelAndView addCard = new ModelAndView("add-card");
        addCard.addObject("card", new Card());
        return addCard;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/add-card")
    public ModelAndView addCard(@Valid @ModelAttribute Card card, BindingResult bindingResult, Model model, Principal principal) {

        if(bindingResult.hasErrors()) {
            return new ModelAndView("redirect:/add-card?binding_errors");
        }

        if (cardService.checkCardExistById(card.getId())) {
            model.addAttribute("error", "Card with the same id already exists!");
            return new ModelAndView("redirect:/add-card?card exist");
        }

        if (cardService.checkCardIsUnique(card.getCardNumber(), card.getCsv())) {
            model.addAttribute("error", "This card is already registered!");
            return new ModelAndView("redirect:/add-card?card exist");
        }

        Card cardToBeSaved = cardService.createCard(card,principal.getName());
        userService.addCardToUser(principal.getName(),cardToBeSaved);

        ModelAndView modelAndView = new ModelAndView("redirect:/add-card?success");

        return modelAndView;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/update-card/{id}")
    public ModelAndView showCardToUpdateForms(@PathVariable int id, Principal principal) {
        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        Card card = cardService.getCardById(id);

//        if (walletService.checkIfWalletNameExist(wallet.getName(), principal.getName())) {
//            return new ModelAndView("redirect:/my-wallets?invalid_wallet_name");
//        }

//        if (cardService.checkCardBelongingByCardNumber(card.getCardNumber(), principal.getName())) {
//            return new ModelAndView("redirect:/my-wallets?this wallet belongs to other");
//        }

        ModelAndView update = new ModelAndView("update-card");

        update.addObject("cardUpdate", dtoMapper.toCardEditDto(card));

        return update;
    }

    @PostMapping("/update-card/{id}")
    public ModelAndView editWallet(@Valid @ModelAttribute("cardUpdate") CardEditDto updatedCard,
                                   @PathVariable int id,
                                   Model model, Principal principal) {
        if (principal == null) {
            return new ModelAndView("/login");
        }

        updatedCard.setUserName(principal.getName());

        cardService.editCard(id, updatedCard);

        return new ModelAndView("redirect:/my-cards");
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("my-cards/delete-card/{card_id}")
    public ModelAndView deleteCard(@PathVariable String card_id, Principal principal) {
        if (principal == null) {
            return new ModelAndView("redirect:/login");
        }

        int cardId = Integer.parseInt(card_id);

//        Card card = cardService.getCardById(cardId);


//        if (walletService.checkIfWalletNameExist(wallet.getName(), principal.getName())) {
//            return new ModelAndView("redirect:/my-wallets?invalid_wallet_name");
//        }

//        if (cardService.checkCardBelongingByCardNumber(card.getCardNumber(), principal.getName())) {
//            return new ModelAndView("redirect:/my-wallets?this wallet belongs to other");
//        }

        cardService.deleteCard(cardId);

        return new ModelAndView("redirect:/my-cards?successful_delete");
    }

}
