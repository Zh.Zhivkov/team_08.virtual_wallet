package com.team08.virtual_wallet.exceptions;

public class EntityExpiredException extends RuntimeException {

    public EntityExpiredException(String message) {
        super(message);
    }

    public EntityExpiredException(String itemType, String attribute, String value) {
        super(String.format("%s with %s expired at %s.", itemType, attribute, value));
    }
}
