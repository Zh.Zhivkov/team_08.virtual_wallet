package com.team08.virtual_wallet.repositories;

import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Page<User> findAllByEnabledTrue(Pageable pageable);

    List<User> findAllByUsernameLike(String username);

    List<User> findAllByPhoneNumberLike(String username);

    List<User> findAllByEmailLike(String username);

    User findUserByUsername(String username);

    User findUserByEmail(String email);

    User findUserByPhoneNumber(String phoneNumber);

    User getUserById(int id);

    List<Wallet> findById(int id);

    boolean existsByUsername(String username);

    boolean existsByPhoneNumber(String username);

    boolean existsByEmail(String username);

    List<User> findAllByUsernameLikeAndEmailLikeAndPhoneNumberLike(String username, String email, String phoneNumber);

    List<User> findAllByBlocked(boolean blocked);

    List<User> findAllByEnabled(boolean enabled);

    List<User> findAllByVerified(boolean verified);
}
