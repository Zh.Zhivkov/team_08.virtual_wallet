package com.team08.virtual_wallet.services;

import com.team08.virtual_wallet.exceptions.DuplicateEntityException;
import com.team08.virtual_wallet.models.GrantedAuthority;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.repositories.AuthorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService {

    private AuthorityRepository authorityRepository;

    @Autowired
    public AuthorityService(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    public GrantedAuthority createAuthority(GrantedAuthority authority) {

        authorityRepository.save(authority);

        return authority;
    }

}
