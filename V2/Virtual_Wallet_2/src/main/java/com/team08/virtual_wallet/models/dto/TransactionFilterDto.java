package com.team08.virtual_wallet.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
@NoArgsConstructor
public class TransactionFilterDto {

    private int senderId;

    private int recipientId;

    private int status;

    private String timePeriod;

    private String sortOption;

    private int direction;

}
