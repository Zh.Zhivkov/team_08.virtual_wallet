package com.team08.virtual_wallet.services;

import com.team08.virtual_wallet.exceptions.EntityDeletedException;
import com.team08.virtual_wallet.exceptions.EntityNotFoundException;
import com.team08.virtual_wallet.models.Card;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import com.team08.virtual_wallet.models.dto.CardEditDto;
import com.team08.virtual_wallet.models.dto.WalletEditDto;
import com.team08.virtual_wallet.repositories.CardRepository;
import com.team08.virtual_wallet.repositories.UserRepository;
import com.team08.virtual_wallet.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;
    private final UserRepository userRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository, UserRepository userRepository) {
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Card getCardById(int id) {
        Card card = cardRepository.getCardById(id);

        if (card == null) {
            throw new EntityNotFoundException("Card", "id", String.valueOf(id));
        }


        return card;
    }

    @Override
    public Card createCard(Card card,String username) {
        User user = userRepository.findUserByUsername(username);
        card.setUser(user);

        card.setEnabled(true);

        return cardRepository.save(card);
    }

    @Override
    public List<Card> getAllCards(String username){
        User user = userRepository.findUserByUsername(username);

        return cardRepository.findAllByUserAndEnabledTrue(user);
    }

    @Override
    public boolean checkCardBelongingByCardNumber(String cardNumber, String username){
        User user = userRepository.findUserByUsername(username);

        return user.getCards().stream()
                .anyMatch(card -> card.getCardNumber().equals(cardNumber));
    }

    @Override
    public boolean checkCardExistById(int id) {
        return cardRepository.existsCardById(id);
    }

    @Override
    public boolean checkCardIsUnique(String cardNumber, String csv) {
        return cardRepository.existsCardByCardNumberAndCsv(cardNumber, csv);
    }

    @Override
    public Card findUserCardByCardNumberContaining(User user, String lastNumbers) {
        return cardRepository.findByUserAndCardNumberLike(user, "%" + lastNumbers + "%");
    }

    @Override
    public Card editCard(int id, CardEditDto cardEditDto) {
        Card cardToBeUpdated = cardRepository.getCardById(id);

        if (cardToBeUpdated == null) {
            throw new EntityDeletedException("Card", "id", String.valueOf(id));
        }

        User user = userRepository.findUserByUsername(cardEditDto.getUserName());

        cardToBeUpdated.setCardholderName(cardEditDto.getCardholderName());
        cardToBeUpdated.setCsv(cardEditDto.getCsv());
        cardToBeUpdated.setCardNumber(cardEditDto.getCardNumber());
        cardToBeUpdated.setExpirationDate(cardEditDto.getExpirationDate());


        cardRepository.save(cardToBeUpdated);

        return cardToBeUpdated;
    }

    @Override
    public void deleteCard(int id) {
        Card card = getCardById(id);

        card.setEnabled(false);

        cardRepository.save(card);

    }

    @Override
    public List<Card> getAllCardsByUser(String name){
        User user = userRepository.findUserByUsername(name);
        return cardRepository.findAllByUserAndEnabledTrue(user);
    }
}
