package com.team08.virtual_wallet.controllers;

import com.google.gson.Gson;
import com.team08.virtual_wallet.models.Transaction;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import com.team08.virtual_wallet.models.dto.DtoMapper;
import com.team08.virtual_wallet.models.dto.TransactionDto;
import com.team08.virtual_wallet.models.dto.TransactionFilterDto;
import com.team08.virtual_wallet.models.dto.UserDto;
import com.team08.virtual_wallet.services.TransactionService;
import com.team08.virtual_wallet.services.UserService;
import com.team08.virtual_wallet.services.WalletServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

@Controller
public class TransactionController {

    private TransactionService transactionService;
    private UserService userService;
    private WalletServiceImpl walletService;
    private DtoMapper dtoMapper;

    @Autowired
    public TransactionController(TransactionService transactionService, UserService userService, WalletServiceImpl walletService, DtoMapper dtoMapper) {
        this.transactionService = transactionService;
        this.userService = userService;
        this.walletService = walletService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/make-transaction")
    public ModelAndView showTransactionPage(Principal principal) {

        if(principal == null) {
            return new ModelAndView("redirect:/login");
        }

        if(!userService.getUserByUsername(principal.getName()).isVerified()) {
            return new ModelAndView("redirect:/my-profile?error_not_verified");
        }

        if(userService.getUserByUsername(principal.getName()).isBlocked()) {
            return new ModelAndView("redirect:/my-profile?error_blocked_transaction");
        }

        ModelAndView page = new ModelAndView("new-make-transaction");
        page.addObject("wallets", userService.getUserByUsername(principal.getName()).getWallets().stream().filter(Wallet::isEnabled).collect(Collectors.toList()));
        page.addObject("transaction", new TransactionDto());
        return page;
    }

    @PostMapping("/make-transaction")
    public ModelAndView makeTransaction(@ModelAttribute @Valid TransactionDto transactionDto, BindingResult bindingResult, Principal principal) {
        if(bindingResult.hasErrors()) {
            return new ModelAndView("redirect:/make-transaction?binding_errors");
        }

        if(principal == null) {
            return new ModelAndView("redirect:/login");
        }

        User sender = userService.getUserByUsername(principal.getName());
        Transaction transaction = dtoMapper.fromTransactionDto(transactionDto);
        transaction.setSender(sender);

        if(!sender.isVerified()) {
            return new ModelAndView("redirect:/my-profile?error_not_verified");
        }

        if(sender.isBlocked()) {
            return new ModelAndView("redirect:/my-profile?error_blocked_transaction");
        }

        if(!sender.getWallets().contains(transaction.getSenderWallet())) {
            return new ModelAndView("redirect:/make-transaction?error_wallet");
        }

        Wallet senderWallet = transaction.getSenderWallet();
        User recipient = transaction.getRecipient();

        if(transaction.getSenderWallet().getBalance() < transaction.getAmount()) {
            transaction.setStatus(0);
            transaction.setDate(LocalDateTime.now());
            transactionService.createTransaction(transaction);
            return new ModelAndView("redirect:/make-transaction?insufficient_balance");
        }

        if(!recipient.isVerified()) {
            transaction.setStatus(0);
            transaction.setDate(LocalDateTime.now());
            transactionService.createTransaction(transaction);
            return new ModelAndView("redirect:/make-transaction?error_recipient_not_verified");
        }

        if(recipient.isBlocked()) {
            transaction.setStatus(0);
            transaction.setDate(LocalDateTime.now());
            transactionService.createTransaction(transaction);
            return new ModelAndView("redirect:/make-transaction?error_recipient_blocked");
        }

        if(recipient.getWallets().isEmpty()) {
            transaction.setStatus(0);
            transaction.setDate(LocalDateTime.now());
            transactionService.createTransaction(transaction);
            return new ModelAndView("redirect:/make-transaction?error_recipient_does_not_have_wallets");
        }

        Wallet recipientWallet = userService.getDefaultUserWallet(recipient);

        senderWallet.setBalance(senderWallet.getBalance() - transaction.getAmount());
        recipientWallet.setBalance(recipientWallet.getBalance() + transaction.getAmount());

        walletService.updateWallet(senderWallet.getId(),senderWallet);
        walletService.updateWallet(recipientWallet.getId(), recipientWallet);

        transaction.setStatus(1);
        transaction.setDate(LocalDateTime.now());
        transactionService.createTransaction(transaction);

        return new ModelAndView("redirect:/make-transaction?success");
    }

    @GetMapping("/transactions")
    public ModelAndView showUserTransactions(Principal principal,
                                             @RequestParam(required = false) String sender,
                                             @RequestParam (required = false) String recipient,
                                             @RequestParam (required = false) String status,
                                             @RequestParam (required = false) String time_period,
                                             @RequestParam (required = false) String sort) {

        if(principal == null) {
            return new ModelAndView("redirect:/login");
        }

        User user = userService.getUserByUsername(principal.getName());
        if (user.getAuthorities().stream().noneMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"))) {
            return new ModelAndView("redirect:/login");
        }

        String[] filterArray = {sender, recipient, status, time_period, sort};

        ModelAndView transactionPage = new ModelAndView("admin-user-transaction");
        transactionPage.addObject("transactionFilter", new TransactionFilterDto());
        transactionPage.addObject("transactions", new Gson().toJson(dtoMapper.transactionDisplayDtos(transactionService.getAllTransactions(filterArray))));
        return transactionPage;
    }

    @PostMapping("/filter-transactions")
    public ModelAndView showFilteredTransactions(@ModelAttribute TransactionFilterDto dto) {
        StringBuilder filterOptions = new StringBuilder("?");
        boolean predecessor = false;

        if(dto.getSenderId() != 0) {
            filterOptions.append(String.format("sender=%d", dto.getSenderId()));
            predecessor = true;
        }

        if(dto.getRecipientId() != 0) {
            if(predecessor) {
                filterOptions.append("&");
            }
            filterOptions.append(String.format("recipient=%d", dto.getRecipientId()));
            predecessor = true;
        }

        if(dto.getStatus() != 0) {
            if(predecessor) {
                filterOptions.append("&");
            }
            filterOptions.append(String.format("status=%d", dto.getStatus() - 1));
            predecessor = true;
        }

        if(dto.getTimePeriod() != null) {
            if(predecessor) {
                filterOptions.append("&");
            }
            filterOptions.append(String.format("time_period=%s", dto.getTimePeriod()));
            predecessor = true;
        }

        if(dto.getSortOption() != null) {
            if(predecessor) {
                filterOptions.append("&");
            }
            filterOptions.append(String.format("sort=%s", dto.getSortOption()));
        }

        return new ModelAndView("redirect:/transactions" + filterOptions.toString());
    }

    @GetMapping("/my-transactions")
    public ModelAndView showMyTransactions(Principal principal,
                                             @RequestParam (required = false) String status,
                                             @RequestParam (required = false) String recipient,
                                             @RequestParam (required = false) String direction,
                                             @RequestParam (required = false) String time_period,
                                             @RequestParam (required = false) String sort) {

        if(principal == null) {
            return new ModelAndView("redirect:/login");
        }

        User user = userService.getUserByUsername(principal.getName());

        String[] filterArray = {direction, recipient, status, time_period, sort};

        ModelAndView transactionPage = new ModelAndView("my-transactions");
        transactionPage.addObject("transactionFilter", new TransactionFilterDto());
        transactionPage.addObject("transactions", new Gson().toJson(dtoMapper.transactionDisplayDtos(user, transactionService.getAllTransactionsOfUser(user, filterArray))));
        return transactionPage;
    }

    @PostMapping("/filter-user-transactions")
    public ModelAndView showFilteredUserTransactions(@ModelAttribute TransactionFilterDto dto) {
        StringBuilder filterOptions = new StringBuilder("?");
        boolean predecessor = false;

        if(dto.getDirection() != 0) {
            filterOptions.append(String.format("direction=%d", dto.getDirection()));
            predecessor = true;
        }

        if(dto.getRecipientId() != 0) {
            if(predecessor) {
                filterOptions.append("&");
            }
            filterOptions.append(String.format("recipient=%d", dto.getRecipientId()));
            predecessor = true;
        }

        if(dto.getStatus() != 0) {
            if(predecessor) {
                filterOptions.append("&");
            }
            filterOptions.append(String.format("status=%d", dto.getStatus() - 1));
            predecessor = true;
        }

        if(dto.getTimePeriod() != null) {
            if(predecessor) {
                filterOptions.append("&");
            }
            filterOptions.append(String.format("time_period=%s", dto.getTimePeriod()));
            predecessor = true;
        }

        if(dto.getSortOption() != null) {
            if(predecessor) {
                filterOptions.append("&");
            }
            filterOptions.append(String.format("sort=%s", dto.getSortOption()));
        }

        return new ModelAndView("redirect:/my-transactions" + filterOptions.toString());
    }
}
