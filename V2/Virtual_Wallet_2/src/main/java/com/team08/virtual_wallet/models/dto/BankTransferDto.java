package com.team08.virtual_wallet.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class BankTransferDto {

    private int amount;

    private String cardNumberLastDigits;

    private int walletId;
}
