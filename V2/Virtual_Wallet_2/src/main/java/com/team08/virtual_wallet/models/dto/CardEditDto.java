package com.team08.virtual_wallet.models.dto;

import com.team08.virtual_wallet.models.User;
import lombok.NoArgsConstructor;
import lombok.Data;

import javax.persistence.Column;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data @NoArgsConstructor
public class CardEditDto {

    private int id;

    @Pattern(regexp = "^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$", message = "Card number must be in xxxx-xxxx-xxxx-xxxx format.")
    @Column(name = "card_number")
    private String cardNumber;

    private String expirationDate;

    @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "Card holder name can only contain letters")
    @Size(min = 2, max = 40, message = "Card holder name is not in permissible size")
    private String cardholderName;

    private String csv;

    private String userName;

}
