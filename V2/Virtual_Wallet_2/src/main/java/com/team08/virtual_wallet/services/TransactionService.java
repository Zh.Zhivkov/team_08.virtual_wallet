package com.team08.virtual_wallet.services;

import com.team08.virtual_wallet.exceptions.EntityNotFoundException;
import com.team08.virtual_wallet.models.Transaction;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.repositories.TransactionRepository;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public List<Transaction> getAllTransactions() {
        List<Transaction> transactions = new ArrayList<>();

        transactionRepository.findAll()
                .forEach(transactions::add);

        return transactions;
    }

    public List<Transaction> getAllTransactions(String[] filterArray) {

        List<Transaction> allTransactions = new ArrayList<>();

        if (filterArray[3] == null) {

            if (filterArray[4] != null) {
                switch (filterArray[4]) {
                    case "amount": {
                        allTransactions = transactionRepository.findAllByOrderByAmountAsc();
                        break;
                    }
                    case "date": {
                        allTransactions = transactionRepository.findAllByOrderByDateAsc();
                        break;
                    }
                    case "amount_desc": {
                        allTransactions = transactionRepository.findAllByOrderByAmountDesc();
                        break;
                    }
                    case "date_desc": {
                        allTransactions = transactionRepository.findAllByOrderByDateDesc();
                        break;
                    }
                }
            } else {
                allTransactions = transactionRepository.findAll();
            }
        } else {

            LocalDateTime pastDate;

            switch (filterArray[3]) {
                case "today": {
                    pastDate = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
                    break;
                }
                case "last_week": {
                    pastDate = LocalDateTime.now().minusDays(7);
                    break;
                }
                case "last_month": {
                    pastDate = LocalDateTime.now().minusMonths(1);
                    break;
                }
                case "last_year": {
                    pastDate = LocalDateTime.now().minusYears(1);
                    break;
                }
                default: {
                    pastDate = LocalDateTime.of(2020,1,1,0,0);
                    break;
                }
            }

            if (filterArray[4] != null) {
                switch (filterArray[4]) {
                    case "amount": {
                        allTransactions = transactionRepository.findAllByDateBetweenOrderByAmountAsc(pastDate, LocalDateTime.now());
                        break;
                    }
                    case "date": {
                        allTransactions = transactionRepository.findAllByDateBetweenOrderByDateAsc(pastDate, LocalDateTime.now());
                        break;
                    }
                    case "amount_desc": {
                        allTransactions = transactionRepository.findAllByDateBetweenOrderByAmountDesc(pastDate, LocalDateTime.now());
                        break;
                    }
                    default: {
                        allTransactions = transactionRepository.findAllByDateBetweenOrderByDateDesc(pastDate, LocalDateTime.now());
                        break;
                    }
                }
            } else {
                allTransactions = transactionRepository.findAllByDateBetweenOrderByDateDesc(pastDate, LocalDateTime.now());
            }

        }

        if(filterArray[0] != null) {
            allTransactions = allTransactions
                    .stream()
                    .filter(transaction -> transaction.getSender().getId() == Integer.parseInt(filterArray[0]))
                    .collect(Collectors.toList());
        }

        if(filterArray[1] != null) {
            allTransactions = allTransactions
                    .stream()
                    .filter(transaction -> transaction.getRecipient().getId() == Integer.parseInt(filterArray[1]))
                    .collect(Collectors.toList());
        }

        if(filterArray[2] != null) {
            allTransactions = allTransactions
                    .stream()
                    .filter(transaction -> transaction.getStatus() == Integer.parseInt(filterArray[2]))
                    .collect(Collectors.toList());
        }

        return allTransactions;
    }

    public List<Transaction> getAllTransactionsOfUser(User user, String[] filterArray) {
        List<Transaction> allTransactions = new ArrayList<>();

        if (filterArray[3] == null) {

            if (filterArray[4] != null) {
                switch (filterArray[4]) {
                    case "amount": {
                        allTransactions = transactionRepository.findAllByOrderByAmountAsc();
                        break;
                    }
                    case "date": {
                        allTransactions = transactionRepository.findAllByOrderByDateAsc();
                        break;
                    }
                    case "amount_desc": {
                        allTransactions = transactionRepository.findAllByOrderByAmountDesc();
                        break;
                    }
                    case "date_desc": {
                        allTransactions = transactionRepository.findAllByOrderByDateDesc();
                        break;
                    }
                }
            } else {
                allTransactions = transactionRepository.findAll();
            }
        } else {

            LocalDateTime pastDate;

            switch (filterArray[3]) {
                case "today": {
                    pastDate = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
                    break;
                }
                case "last_week": {
                    pastDate = LocalDateTime.now().minusDays(7);
                    break;
                }
                case "last_month": {
                    pastDate = LocalDateTime.now().minusMonths(1);
                    break;
                }
                case "last_year": {
                    pastDate = LocalDateTime.now().minusYears(1);
                    break;
                }
                default: {
                    pastDate = LocalDateTime.of(2020,1,1,0,0);
                    break;
                }
            }

            if (filterArray[4] != null) {
                switch (filterArray[4]) {
                    case "amount": {
                        allTransactions = transactionRepository.findAllByDateBetweenOrderByAmountAsc(pastDate, LocalDateTime.now());
                        break;
                    }
                    case "date": {
                        allTransactions = transactionRepository.findAllByDateBetweenOrderByDateAsc(pastDate, LocalDateTime.now());
                        break;
                    }
                    case "amount_desc": {
                        allTransactions = transactionRepository.findAllByDateBetweenOrderByAmountDesc(pastDate, LocalDateTime.now());
                        break;
                    }
                    default: {
                        allTransactions = transactionRepository.findAllByDateBetweenOrderByDateDesc(pastDate, LocalDateTime.now());
                        break;
                    }
                }
            } else {
                allTransactions = transactionRepository.findAllByDateBetweenOrderByDateDesc(pastDate, LocalDateTime.now());
            }
        }

        if(filterArray[0] == null) {
            allTransactions = allTransactions
                    .stream()
                    .filter(transaction -> transaction.getSender().getId() == user.getId() || transaction.getRecipient().getId() == user.getId())
                    .collect(Collectors.toList());
        }

        if(filterArray[0] != null) {
            if(filterArray[0].equals("0")) {
                allTransactions = allTransactions
                        .stream()
                        .filter(transaction -> transaction.getSender().getId() == user.getId() || transaction.getRecipient().getId() == user.getId())
                        .collect(Collectors.toList());
            }
            if (filterArray[0].equals("1")) {
                allTransactions = allTransactions
                        .stream()
                        .filter(transaction -> transaction.getRecipient().getId() == user.getId())
                        .collect(Collectors.toList());
            }
            if (filterArray[0].equals("2")) {
                allTransactions = allTransactions
                        .stream()
                        .filter(transaction -> transaction.getSender().getId() == user.getId())
                        .collect(Collectors.toList());
            }
        }

        if(filterArray[1] != null) {
            allTransactions = allTransactions
                    .stream()
                    .filter(transaction -> transaction.getRecipient().getId() == Integer.parseInt(filterArray[1]))
                    .collect(Collectors.toList());
        }

        if(filterArray[2] != null) {
            allTransactions = allTransactions
                    .stream()
                    .filter(transaction -> transaction.getStatus() == Integer.parseInt(filterArray[2]))
                    .collect(Collectors.toList());
        }

        return allTransactions;
    }

    public Transaction createTransaction(Transaction transaction) {
//        transactionRepository.findById(transaction.getId())
//                .orElseThrow(() -> new EntityNotFoundException("Transaction", "id", String.valueOf(transaction.getId())));

        transactionRepository.save(transaction);

        return transaction;
    }

}
