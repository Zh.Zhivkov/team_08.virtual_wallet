package com.team08.virtual_wallet.services.contracts;

import com.team08.virtual_wallet.models.BankTransfer;
import com.team08.virtual_wallet.models.User;
import com.team08.virtual_wallet.models.Wallet;
import com.team08.virtual_wallet.models.dto.WalletEditDto;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;

import java.util.List;

public interface WalletService {

//    List<Wallet> getAllWallets();

    Wallet createWallet(Wallet wallet,String username);

    Wallet getWalletById(int id);

    Wallet getWalletByName(String name);

    Wallet editWallet(int id,WalletEditDto walletEditDto);

    Wallet updateWallet(int id, Wallet wallet);

    void deleteWallet(int id,String username);

    void transferMoneyFromCardToWallet(int amount, int walletId);

    HttpStatus requestToApi(BankTransfer bankTransfer);

    List<Wallet> getAllWallets(User user, String[] filterArray);

    boolean checkIfWalletNameExist(String walletName, String username);

    boolean checkWalletBelongingByName(String walletName, String username);

    void setDefaultWallet(Wallet oldDefault, int newDefaultId);
}
