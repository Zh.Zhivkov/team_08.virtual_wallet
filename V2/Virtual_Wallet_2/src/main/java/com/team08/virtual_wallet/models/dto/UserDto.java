package com.team08.virtual_wallet.models.dto;

import com.team08.virtual_wallet.models.Wallet;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {

    private int id;

    @Size(min = 4, message = "Username is required")
    @NotBlank
    @NotNull
    private String username;

    @Size(min = 1)
    @Email(message = "Invalid email")
    @NotNull
    private String   email;

    @NotNull
    private String phoneNumber;

    @Size(min = 1, message = "Password is required")
    @NotBlank
    @NotNull
    private String password;

    private String passwordConfirmation;

    private boolean enabled;

    private String photo;

    private MultipartFile photoUpdate;

    private List<Wallet> wallets;

    private boolean blocked;

    private boolean verified;

    private String firstName;
    private String lastName;

    public UserDto(String username, String email, String phoneNumber, String password) {
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }
}
