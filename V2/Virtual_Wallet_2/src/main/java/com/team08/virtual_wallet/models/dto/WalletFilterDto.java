package com.team08.virtual_wallet.models.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WalletFilterDto {

    private String name;

    private String sortOption;

}
