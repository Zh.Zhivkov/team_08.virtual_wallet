package com.team08.virtual_wallet.repositories;

import com.team08.virtual_wallet.models.GrantedAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<GrantedAuthority, Integer> {

}
