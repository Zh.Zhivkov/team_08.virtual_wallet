package com.team08.virtual_wallet.exceptions;

public class EntityDeletedException extends RuntimeException {

    public EntityDeletedException(String message) {
        super(message);
    }

    public EntityDeletedException(String itemType, String attribute, String value) {
        super(String.format("%s with %s: %s is deleted.", itemType, attribute, value));
    }
}
